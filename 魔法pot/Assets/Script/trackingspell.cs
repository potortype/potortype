﻿using UnityEngine;
using System.Collections;

public class trackingspell : MonoBehaviour
{

    //目標
    public GameObject currenttarget;
    public GameObject finaltarget;
    //
    //public Vector3 Desiredvelocity;
    public Vector3 currentvelocity;
    //public Vector3 Seeksteering;
    public int dam;
    public int CasterID;
    public float currentspeed;
    public int currentpathmagic;
    public float predictionfactor = 0.00001f;
    public Vector3 predictionpos;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        currentvelocity = this.gameObject.transform.forward * currentspeed;
        //Debug.Log("this.gameObject.transform.forward" + this.gameObject.transform.forward * 0.1f);
        //當當前目標被銷毀的時候
        if (currenttarget.Equals(null))
        {
            findpathspell();
            //if (MagicDamHandle._Instance.allRBpathmagic.Count == 0)
            //{
            //    currenttarget = finaltarget;
            //    Debug.Log("path上沒有法術，法術目標為人 = " + finaltarget);
            //}
            //else if (MagicDamHandle._Instance.allRBpathmagic.Count != 0 && currenttarget != finaltarget)
            //{
            //    for (int i = 0; i < MagicDamHandle._Instance.allRBpathmagic.Count; i++)
            //    {
            //        if (MagicDamHandle._Instance.allRBpathmagic[i].GetComponent<trackingspell>().CasterID != CasterID)
            //        {
            //            currenttarget = MagicDamHandle._Instance.allRBpathmagic[i];
            //            Debug.Log("path上有敵方法術，設定為當前seek目標 = " + currenttarget);
            //        }
            //        else { continue; }
            //        break;
            //    }
            //}
            //Debug.Log("當前目標被銷毀，更換當前目標為 = " + currenttarget);
        }
        if (currenttarget != finaltarget)
        {
            //Debug.Log("攔截");
            prediction(this.gameObject);
            //目標是法術做攔截
        }
        else
        {
            //Debug.Log("seek");
            Seek(this.gameObject);
            //目標是人做seek
        }
        //只要傷害被扣到0就自毀，且傷害計算結束
        if ((dam <= 0)/* && (Calculatedown)*/)
        {
            //if (Calculatedown1 || Calculatedown2)
            //{
            MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
            Destroy(this.gameObject);

            //}
        }

        //當有人施放法術的
        //if(currentpathmagic != MagicDamHandle._Instance.allRBpathmagic.Count)
        //{
        //    //當法術的目標不是人的時候
        //    if(currenttarget != finaltarget)
        //    {
        //        //重新搜尋法術
        //        for (int i = 0; i < MagicDamHandle._Instance.allRBpathmagic.Count; i++)
        //        {
        //            if (MagicDamHandle._Instance.allRBpathmagic[i].GetComponent<trackingspell>().CasterID != CasterID)
        //            {
        //                currenttarget = MagicDamHandle._Instance.allRBpathmagic[i];
        //                Debug.Log("飛行途中path上還有敵方法術，設定為當前seek目標 = " + currenttarget);
        //            }
        //            else { continue; }
        //        }
        //    }
        //}

    }
    public void Seek(GameObject spell)
    {
        Vector3 Seekposition = currenttarget.transform.position;
        Vector3 Unitdesiredvelocity;
        Vector3 Desiredvelocity;
        Vector3 Seeksteering;
        //求出seeksteering
        //根據desirevelocity(向量) = normalize(target - position)(單位向量) * maxspeed(速度)
        //maxspeed也可以說是desirevelocity的長度，因為desirevelocity必須是定值，表示該NPC的最大速度上限
        //根據steerting(向量) = desirevelocity(向量) - NPC.transform.forward(單位向量) * CurrentSpeed(速度)
        Unitdesiredvelocity = (Seekposition - spell.transform.position);
        //Unitdesiredvelocity.y = 0;
        Unitdesiredvelocity.Normalize();
        Desiredvelocity = Unitdesiredvelocity * MagicDamHandle._Instance.maxspellspeed;
        Seeksteering = Desiredvelocity - (spell.transform.forward * currentspeed);
        //找出兩物距離dist
        //並且防呆，距離小於NPC的CurrentSpeed，表示Seek到目的地了，把CurrentSpeed歸零
        float dist = (Seekposition - spell.transform.position).magnitude;
        if (dist < currentspeed * Time.deltaTime)
        {
            spell.transform.position = Seekposition;
            currentspeed = 0;
            return;
        }
        //分解seeksteering以求出NPC的AccelerationSpeed(加速度向量)、TurnForce(轉向力向量)
        Vector3 AccelerationSpeed = Vector3.Project(Seeksteering, spell.transform.forward);
        Vector3 TurnForce = Seeksteering - AccelerationSpeed;
        //計算seek轉向
        //把目前NPC的forward(單位向量) + TurnForce(轉向力向量) * Time.deltaTime (用Time.deltaTime限制NPC每一次轉向角度避免Seek轉向角度過大造成BUG)
        //因為三角形的兩邊相加 > 第三邊，所以turningvelocity(向量)已經不是1，必須再取一次Normalize，放回去NPC的forward
        Vector3 turningvelocity = spell.transform.forward + TurnForce * /*Time.deltaTime*/MagicDamHandle._Instance.maxRotate;
        turningvelocity.Normalize();
        spell.transform.forward = turningvelocity;
        //計算seek速度
        //計算CurrentSpeed = AccelerationSpeed(向量)的長度 * Time.deltaTime((用Time.deltaTime限制NPC每一次加速度的大小)
        currentspeed += AccelerationSpeed.magnitude * Time.deltaTime;
        //Debug.Log(currentspeed);
        //NPC的速度應該不能超過他的最大速度
        if (currentspeed > MagicDamHandle._Instance.maxspellspeed)
        {
            currentspeed = MagicDamHandle._Instance.maxspellspeed;
        }
        //NPC移動
        spell.transform.position += spell.transform.forward * currentspeed * Time.deltaTime;
    }

    public void prediction(GameObject spell)
    {
        //攔截就是把目標的currentvelocity推算一個時間
        Vector3 predictionpos = currenttarget.transform.position + currenttarget.transform.forward.normalized * predictionfactor;
        Debug.Log("predictionpos = " + predictionpos);
        Vector3 Unitdesiredvelocity;
        Vector3 Desiredvelocity;
        Vector3 Seeksteering;
        //求出seeksteering
        //根據desirevelocity(向量) = normalize(target - position)(單位向量) * maxspeed(速度)
        //maxspeed也可以說是desirevelocity的長度，因為desirevelocity必須是定值，表示該NPC的最大速度上限
        //根據steerting(向量) = desirevelocity(向量) - NPC.transform.forward(單位向量) * CurrentSpeed(速度)
        Unitdesiredvelocity = (predictionpos - spell.transform.position);
        //Unitdesiredvelocity.y = 0;
        Unitdesiredvelocity.Normalize();
        Desiredvelocity = Unitdesiredvelocity * MagicDamHandle._Instance.maxpredictionspeed;
        Seeksteering = Desiredvelocity - (spell.transform.forward * currentspeed);
        //找出兩物距離dist
        //並且防呆，距離小於NPC的CurrentSpeed，表示Seek到目的地了，把CurrentSpeed歸零
        float dist = (predictionpos - spell.transform.position).magnitude;
        if (dist < currentspeed * Time.deltaTime)
        {
            spell.transform.position = predictionpos;
            currentspeed = 0;
            return;
        }
        //分解seeksteering以求出NPC的AccelerationSpeed(加速度向量)、TurnForce(轉向力向量)
        Vector3 AccelerationSpeed = Vector3.Project(Seeksteering, spell.transform.forward);
        Vector3 TurnForce = Seeksteering - AccelerationSpeed;
        //計算seek轉向
        //把目前NPC的forward(單位向量) + TurnForce(轉向力向量) * Time.deltaTime (用Time.deltaTime限制NPC每一次轉向角度避免Seek轉向角度過大造成BUG)
        //因為三角形的兩邊相加 > 第三邊，所以turningvelocity(向量)已經不是1，必須再取一次Normalize，放回去NPC的forward
        Vector3 turningvelocity = spell.transform.forward + TurnForce * /*Time.deltaTime*/MagicDamHandle._Instance.maxRotate;
        turningvelocity.Normalize();
        spell.transform.forward = turningvelocity;
        //計算seek速度
        //計算CurrentSpeed = AccelerationSpeed(向量)的長度 * Time.deltaTime((用Time.deltaTime限制NPC每一次加速度的大小)
        currentspeed += AccelerationSpeed.magnitude * Time.deltaTime;
        //Debug.Log(currentspeed);
        //NPC的速度應該不能超過他的最大速度
        if (currentspeed > MagicDamHandle._Instance.maxpredictionspeed)
        {
            currentspeed = MagicDamHandle._Instance.maxpredictionspeed;
        }
        //NPC移動
        spell.transform.position += spell.transform.forward * currentspeed * Time.deltaTime;
    }
    //不管如何法術碰撞後(穿透或抵消)，都在執行一次
    void findpathspell()
    {
        //如果路徑上沒有法術
        if (MagicDamHandle._Instance.allRBpathmagic.Count == 0)
        {
            //就去追人
            currenttarget = finaltarget;
            Debug.Log("path上沒有法術，法術目標為人 = " + finaltarget);
        }
        //當路徑上有任何法術，而且當前目標不是人的時候(也就是當要追蹤的法術的時候，避免要去追人(互穿)的法術回頭追另一個法術)
        else if (MagicDamHandle._Instance.allRBpathmagic.Count != 0 && currenttarget != finaltarget)
        {
            for (int i = 0; i < MagicDamHandle._Instance.allRBpathmagic.Count; i++)
            {
                //當有敵人的法術存在
                if (MagicDamHandle._Instance.allRBpathmagic[i].GetComponent<trackingspell>().CasterID != CasterID)
                {
                    //去追那個法術
                    currenttarget = MagicDamHandle._Instance.allRBpathmagic[i];
                    Debug.Log("path上有敵方法術，設定為當前seek目標 = " + currenttarget);
                }
                else { continue; }
                //break;
            }
        }
        Debug.Log("更換當前目標為 = " + currenttarget);
        return;
    }
    //void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawWireSphere(predictionpos, 0.1f);
    //    Gizmos.color = Color.green;
    //    Gizmos.DrawLine(this.gameObject.transform.position, currentvelocity);
    //    Gizmos.color = Color.blue;
    //    Gizmos.DrawLine(this.gameObject.transform.position, this.gameObject.transform.forward * 1.0f);

    //}
    void OnTriggerEnter(Collider collider)
    {
        //先不管撞到就消失
        //Destroy(collider);
        if (collider.name.Equals("Target"))
            Destroy(this.gameObject);
        if (collider.name.Equals("Wall"))
            Destroy(this.gameObject);

        if (CasterID == 1 && collider.name.Contains("spell"))
        {

            //小火
            if (gameObject.name.Equals("spellfireballS(Clone)"))
            {
                if (collider.name.Equals("spellfireballS(Clone)"))
                {
                    ////Debug.Log("小火球撞到 = " + collider.name);
                    ////Debug.Log("同屬性互相抵銷");
                    //int mydam = dam;
                    ////Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    ////Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spellfireballL(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("同屬性強度比他弱");
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spellwaterballS(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("互剋同強度");
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spellwaterballL(Clone)"))
                {
                    ////Debug.Log("小火球撞到 = " + collider.name);
                    ////Debug.Log("互剋比他弱");
                    //int mydam = dam;
                    ////Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    ////Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spelllightballS(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelllightballL(Clone)"))
                {
                    Debug.Log("小火球撞到 = " + collider.name);
                    Debug.Log("互剋比他弱");
                    // int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelldarkballS(Clone)"))
                {
                    Debug.Log("小火球撞到 = " + collider.name);
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelldarkballL(Clone)"))
                {
                    Debug.Log("小火球撞到 = " + collider.name);
                    Debug.Log("互剋比他弱");
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixS(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("互剋比他弱");
                    int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixL(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("互剋比他弱");
                    int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                //蹦撞後，再去搜尋一次路徑有沒有法術
                findpathspell();
                //currenttarget = null;
                //if (currenttarget == null)
                //{
                //    Debug.Log("追蹤的法術被銷毀");
                //    currenttarget = finaltarget;
                //    Debug.Log("更換當前目標 = " + currenttarget);
                //}
                // Calculatedown1 = true;
            }

            //大火
            if (gameObject.name.Equals("spellfireballL(Clone)"))
            {
                if (collider.name.Equals("spellfireballS(Clone)"))
                {
                    ////Debug.Log("小火球撞到 = " + collider.name);
                    ////Debug.Log("同屬性互相抵銷");
                    //int mydam = dam;
                    ////Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    ////Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spellfireballL(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("同屬性強度比他弱");
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spellwaterballS(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("互剋同強度");
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spellwaterballL(Clone)"))
                {
                    ////Debug.Log("小火球撞到 = " + collider.name);
                    ////Debug.Log("互剋比他弱");
                    //int mydam = dam;
                    ////Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    ////Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spelllightballS(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelllightballL(Clone)"))
                {
                    Debug.Log("小火球撞到 = " + collider.name);
                    Debug.Log("互剋比他弱");
                    // int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelldarkballS(Clone)"))
                {
                    Debug.Log("小火球撞到 = " + collider.name);
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelldarkballL(Clone)"))
                {
                    Debug.Log("小火球撞到 = " + collider.name);
                    Debug.Log("互剋比他弱");
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixS(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("互剋比他弱");
                    int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixL(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("互剋比他弱");
                    int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                findpathspell();
                //currenttarget = null;
                //if (currenttarget == null)
                //{
                //    Debug.Log("追蹤的法術被銷毀");
                //    currenttarget = finaltarget;
                //    Debug.Log("更換當前目標 = " + currenttarget);
                //}
                // Calculatedown1 = true;
            }
            //小水
            if (gameObject.name.Equals("spellwaterballS(Clone)"))
            {
                if (collider.name.Equals("spellfireballS(Clone)"))
                {
                    ////Debug.Log("小火球撞到 = " + collider.name);
                    ////Debug.Log("同屬性互相抵銷");
                    //int mydam = dam;
                    ////Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    ////Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spellfireballL(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("同屬性強度比他弱");
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spellwaterballS(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("互剋同強度");
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    //Destroy(this.gameObject);
                    //Destroy(collider.gameObject);
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spellwaterballL(Clone)"))
                {
                    ////Debug.Log("小火球撞到 = " + collider.name);
                    ////Debug.Log("互剋比他弱");
                    //int mydam = dam;
                    ////Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    ////Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    //Destroy(this.gameObject);
                    //Destroy(collider.gameObject);
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spelllightballS(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelllightballL(Clone)"))
                {
                    Debug.Log("小火球撞到 = " + collider.name);
                    Debug.Log("互剋比他弱");
                    // int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelldarkballS(Clone)"))
                {
                    Debug.Log("小火球撞到 = " + collider.name);
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelldarkballL(Clone)"))
                {
                    Debug.Log("小火球撞到 = " + collider.name);
                    Debug.Log("互剋比他弱");
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixS(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("互剋比他弱");
                    int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixL(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("互剋比他弱");
                    int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                findpathspell();
                //currenttarget = null;
                //if (currenttarget == null)
                //{
                //    Debug.Log("追蹤的法術被銷毀");
                //    currenttarget = finaltarget;
                //    Debug.Log("更換當前目標 = " + currenttarget);
                //}
                // Calculatedown1 = true;
            }
            //大水--------------------------------------------------
            if (gameObject.name.Equals("spellwaterballL(Clone)"))
            {
                if (collider.name.Equals("spellfireballS(Clone)"))
                {
                    ////Debug.Log("小火球撞到 = " + collider.name);
                    ////Debug.Log("同屬性互相抵銷");
                    //int mydam = dam;
                    ////Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    ////Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spellfireballL(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("同屬性強度比他弱");
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spellwaterballS(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("互剋同強度");
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    //Destroy(this.gameObject);
                    //Destroy(collider.gameObject);
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spellwaterballL(Clone)"))
                {
                    ////Debug.Log("小火球撞到 = " + collider.name);
                    ////Debug.Log("互剋比他弱");
                    //int mydam = dam;
                    ////Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    ////Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    //Destroy(this.gameObject);
                    //Destroy(collider.gameObject);
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spelllightballS(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelllightballL(Clone)"))
                {
                    Debug.Log("小火球撞到 = " + collider.name);
                    Debug.Log("互剋比他弱");
                    // int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelldarkballS(Clone)"))
                {
                    Debug.Log("小火球撞到 = " + collider.name);
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelldarkballL(Clone)"))
                {
                    Debug.Log("小火球撞到 = " + collider.name);
                    Debug.Log("互剋比他弱");
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    //collider.GetComponent<trackingspell>().dam = you;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixS(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("互剋比他弱");
                    int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixL(Clone)"))
                {
                    //Debug.Log("小火球撞到 = " + collider.name);
                    //Debug.Log("互剋比他弱");
                    int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                findpathspell();
                //currenttarget = null;
                //if (currenttarget == null)
                //{
                //    Debug.Log("追蹤的法術被銷毀");
                //    currenttarget = finaltarget;
                //    Debug.Log("更換當前目標 = " + currenttarget);
                //}
                // Calculatedown1 = true;
            }
            //小光
            if (gameObject.name.Equals("spelllightballS(Clone)"))
            {
                if (collider.name.Equals("spellfireballS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellfireballL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellwaterballS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellwaterballL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelllightballS(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spelllightballL(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spelldarkballS(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spelldarkballL(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spellPhoenixL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }

                findpathspell();
                //currenttarget = null;
                //if (currenttarget == null)
                //{
                //    Debug.Log("追蹤的法術被銷毀");
                //    currenttarget = finaltarget;
                //    Debug.Log("更換當前目標 = " + currenttarget);
                //}// Calculatedown1 = true;
            }

            //大光
            if (gameObject.name.Equals("spelllightballL(Clone)"))
            {
                if (collider.name.Equals("spellfireballS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellfireballL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellwaterballS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellwaterballL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelllightballS(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spelllightballL(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spelldarkballS(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spelldarkballL(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spellPhoenixL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }

                findpathspell();
                //currenttarget = null;
                //if (currenttarget == null)
                //{
                //    Debug.Log("追蹤的法術被銷毀");
                //    currenttarget = finaltarget;
                //    Debug.Log("更換當前目標 = " + currenttarget);
                //}// Calculatedown1 = true;
            }
            //小暗
            if (gameObject.name.Equals("spelldarkballS(Clone)"))
            {
                if (collider.name.Equals("spellfireballS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellfireballL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellwaterballS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellwaterballL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelllightballS(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spelllightballL(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spelldarkballS(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    //Destroy(this.gameObject);
                    //Destroy(collider.gameObject);
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spelldarkballL(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    //Destroy(this.gameObject);
                    //Destroy(collider.gameObject);
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spellPhoenixL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }

                findpathspell();
                //currenttarget = null;
                //if (currenttarget == null)
                //{
                //    Debug.Log("追蹤的法術被銷毀");
                //    currenttarget = finaltarget;
                //    Debug.Log("更換當前目標 = " + currenttarget);
                //}// Calculatedown1 = true;
            }
            //大暗
            if (gameObject.name.Equals("spelldarkballL(Clone)"))
            {
                if (collider.name.Equals("spellfireballS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellfireballL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellwaterballS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellwaterballL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelllightballS(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spelllightballL(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    MagicDamHandle._Instance.allRBpathmagic.Remove(this.gameObject);
                    MagicDamHandle._Instance.allRBpathmagic.Remove(collider.gameObject);
                    Destroy(this.gameObject);
                    Destroy(collider.gameObject);
                }
                if (collider.name.Equals("spelldarkballS(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    //Destroy(this.gameObject);
                    //Destroy(collider.gameObject);
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spelldarkballL(Clone)"))
                {
                    //int mydam = dam;
                    //Debug.Log("mydam" + mydam);
                    //int you = (collider.GetComponent<trackingspell>().dam);
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    //Destroy(this.gameObject);
                    //Destroy(collider.gameObject);
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spellPhoenixL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }

                findpathspell();
                //currenttarget = null;
                //if (currenttarget == null)
                //{
                //    Debug.Log("追蹤的法術被銷毀");
                //    currenttarget = finaltarget;
                //    Debug.Log("更換當前目標 = " + currenttarget);
                //}// Calculatedown1 = true;
            }
            //大鳳
            if (gameObject.name.Equals("spellPhoenixL(Clone)"))
            {
                if (collider.name.Equals("spellfireballS(Clone)"))
                {
                    int mydam = (dam);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellfireballL(Clone)"))
                {
                    int mydam = (dam);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellwaterballS(Clone)"))
                {
                    //int magicReducing = 5;
                    int mydam = (dam /*- magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam/* - magicReducing*/;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellwaterballL(Clone)"))
                {
                    //int magicReducing = 5;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam/* - magicReducing*/;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelllightballS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelllightballL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelldarkballS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelldarkballL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixL(Clone)"))
                {
                    //int mydam = (dam);
                    //dam = mydam;
                    //Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spellPhoenixS(Clone)"))
                {
                    //int mydam = (dam);
                    //dam = mydam;
                    //Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                findpathspell();
                //currenttarget = null;
                //if (currenttarget == null)
                //{
                //    Debug.Log("追蹤的法術被銷毀");
                //    currenttarget = finaltarget;
                //    Debug.Log("更換當前目標 = " + currenttarget);
                //}
                //Calculatedown1 = true;
            }


            //小鳳
            if (gameObject.name.Equals("spellPhoenixS(Clone)"))
            {
                if (collider.name.Equals("spellfireballS(Clone)"))
                {
                    int mydam = (dam);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellfireballL(Clone)"))
                {
                    int mydam = (dam);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellwaterballS(Clone)"))
                {
                    //int magicReducing = 5;
                    int mydam = (dam /*- magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam/* - magicReducing*/;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellwaterballL(Clone)"))
                {
                    //int magicReducing = 5;
                    int mydam = (dam/* - magicReducing*/);
                    dam = mydam;
                    Debug.Log("mydam" + mydam);
                    int you = collider.GetComponent<trackingspell>().dam/* - magicReducing*/;
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelllightballS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelllightballL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelldarkballS(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spelldarkballL(Clone)"))
                {
                    //int magicReducing = 10;
                    int mydam = dam;
                    Debug.Log("mydam" + mydam);
                    int you = (collider.GetComponent<trackingspell>().dam/* - magicReducing*/);
                    Debug.Log("you" + you);
                    collider.GetComponent<trackingspell>().dam -= mydam;
                    dam -= you;
                }
                if (collider.name.Equals("spellPhoenixL(Clone)"))
                {
                    //int mydam = (dam);
                    //dam = mydam;
                    //Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                if (collider.name.Equals("spellPhoenixS(Clone)"))
                {
                    //int mydam = (dam);
                    //dam = mydam;
                    //Debug.Log("mydam" + mydam);
                    //int you = collider.GetComponent<trackingspell>().dam;
                    //Debug.Log("you" + you);
                    //collider.GetComponent<trackingspell>().dam -= mydam;
                    //dam -= you;
                    gameObject.GetComponent<trackingspell>().currenttarget = finaltarget;
                    collider.GetComponent<trackingspell>().currenttarget = collider.GetComponent<trackingspell>().finaltarget;
                }
                findpathspell();
                //currenttarget = null;
                                //if (currenttarget == null)
                                //{
                                //    Debug.Log("追蹤的法術被銷毀");
                                //    currenttarget = finaltarget;
                                //    Debug.Log("更換當前目標 = " + currenttarget);
                                //}
                                // Calculatedown1 = true;
            }
        }

        //我只要碰到法術就切換目標，反正死掉的會死掉，活下來的就繼續seek
    }

}
