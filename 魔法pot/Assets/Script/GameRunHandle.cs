﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameRunHandle : MonoBehaviour {
    static public GameRunHandle _Instance;
    public List<GameObject> AllWalls;
    public List<GameObject> BlueTeam;
    public List<GameObject> RedTeam;
    public List<GameObject> AllPoints;
    public float PlayerHP = 10000;
    public int startpoint = 5;
    public float increacepointtime = 1.0f;
    public int maxpoint = 10;
    public float pointcount = 0.0f;
    //先用gameobject，之後改用VRplay
    //public List<GameObject> PlayerList;
    //public GameObject[] PlayerList;
    //AI用
    //public GameObject playusespell;
    void Awake()
    {
        _Instance = this;
        //實體化List 
        NewAllList();       
        //獲取場景中所有的WALL
        SetAllLists();
        //PlayerList = new List<GameObject>();
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //pointcount += Time.deltaTime;

        //if(startpoint < maxpoint)
        //{
        //    if (pointcount >= increacepointtime)
        //    {
        //        //去找所有人加點數
        //        pointcount = 0.0f;
        //        startpoint++;

        //        PlayerList[0].GetComponent<VRplayer>().startPoint = startpoint;
        //        PlayerList[1].GetComponent<AImagica>().startpoint = startpoint;
        //    }
        //}
        //else
        //{
        //    pointcount = 0.0f;
        //}
    }
    void SetAllLists()
    {
        GameObject[] Walls = GameObject.FindGameObjectsWithTag("Wall");
        for(int i = 0; i < Walls.Length; i++)
        {
            AllWalls.Add(Walls[i]);
        }
        Debug.Log("Set Walls Done");
        Debug.Log("Total Have-- " + AllWalls.Count + " --Walls");
        GameObject[] Points = GameObject.FindGameObjectsWithTag("Point");
        for (int i = 0; i < Points.Length; i++)
        {
            AllPoints.Add(Points[i]);
        }
        Debug.Log("Set Points Done");
        Debug.Log("Total Have-- " + AllPoints.Count + " --Points");
    }
    void NewAllList()
    {
        AllWalls = new List<GameObject>();
        BlueTeam = new List<GameObject>();
        RedTeam = new List<GameObject>();
        AllPoints = new List<GameObject>();
    }
}
