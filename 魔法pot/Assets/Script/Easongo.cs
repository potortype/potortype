﻿using UnityEngine;
using System.Collections;

public class Easongo : MonoBehaviour {
    
    //public float speed = 1.0f;
    public Vector3 target;
    public bool go = false;
    public float count = 0.0f;
    public float tt = 6.0f;
    //public GameObject eeee;
    public Vector3 from;
    public bool arrival = true;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (go)
        {
            count += Time.deltaTime;
            Debug.Log("人物的計時 =  " + count);
            // this.gameObject.transform.forward = target - this.gameObject.transform.position.normalized;
            //Seek(eeee);
            this.gameObject.transform.position = Vector3.Lerp(from, target, count / tt);
            if (count >= tt)
            {
                count = 0.0f;
                go = false;
                arrival = true;
            }
        }
        else
        {
            count = 0.0f;
            
        }

        if (Vector3.Distance(this.gameObject.transform.position , target) <= 0.5f && go)
        {
            arrival = true;
            go = false;
            this.gameObject.transform.position = target;
            CameraController._Instance.goback();
        }

    }
    //void gogo(GameObject ee)
    //{
    //    gameObject.transform.position += ee.gameObject.transform.forward * speed;
    //}
    //public void Seek(GameObject spell)
    //{
    //    Vector3 Seekposition = target;
    //    Vector3 Unitdesiredvelocity;
    //    Vector3 Desiredvelocity;
    //    Vector3 Seeksteering;
    //    //求出seeksteering
    //    //根據desirevelocity(向量) = normalize(target - position)(單位向量) * maxspeed(速度)
    //    //maxspeed也可以說是desirevelocity的長度，因為desirevelocity必須是定值，表示該NPC的最大速度上限
    //    //根據steerting(向量) = desirevelocity(向量) - NPC.transform.forward(單位向量) * CurrentSpeed(速度)
    //    Unitdesiredvelocity = (Seekposition - spell.transform.position);
    //    //Unitdesiredvelocity.y = 0;
    //    Unitdesiredvelocity.Normalize();
    //    Desiredvelocity = Unitdesiredvelocity * MacSpeed;
    //    Seeksteering = Desiredvelocity - (spell.transform.forward * CurrentSpeed);
    //    //找出兩物距離dist
    //    //並且防呆，距離小於NPC的CurrentSpeed，表示Seek到目的地了，把CurrentSpeed歸零
    //    float dist = (Seekposition - spell.transform.position).magnitude;
    //    if (dist < speed * Time.deltaTime)
    //    {
    //        spell.transform.position = Seekposition;
    //        speed = 0;
    //        return;
    //    }
    //    //分解seeksteering以求出NPC的AccelerationSpeed(加速度向量)、TurnForce(轉向力向量)
    //    Vector3 AccelerationSpeed = Vector3.Project(Seeksteering, spell.transform.forward);
    //    Vector3 TurnForce = Seeksteering - AccelerationSpeed;
    //    //計算seek轉向
    //    //把目前NPC的forward(單位向量) + TurnForce(轉向力向量) * Time.deltaTime (用Time.deltaTime限制NPC每一次轉向角度避免Seek轉向角度過大造成BUG)
    //    //因為三角形的兩邊相加 > 第三邊，所以turningvelocity(向量)已經不是1，必須再取一次Normalize，放回去NPC的forward
    //    Vector3 turningvelocity = spell.transform.forward + TurnForce * Time.deltaTime;
    //    turningvelocity.Normalize();
    //    spell.transform.forward = turningvelocity;
    //    //計算seek速度
    //    //計算CurrentSpeed = AccelerationSpeed(向量)的長度 * Time.deltaTime((用Time.deltaTime限制NPC每一次加速度的大小)
    //    speed += AccelerationSpeed.magnitude * Time.deltaTime;
    //    //Debug.Log(currentspeed);
    //    //NPC的速度應該不能超過他的最大速度
    //    if (speed > 3.0f)
    //    {
    //        speed = 3.0f;
    //    }
    //    //NPC移動
    //    spell.transform.position += spell.transform.forward * speed * Time.deltaTime;
    //}
}
