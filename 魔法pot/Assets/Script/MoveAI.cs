﻿using UnityEngine;
using System.Collections;

public class MoveAI : MonoBehaviour {
    public GameObject outside;
    public GameObject inside;
    public float count;
    public float count1;
    public float t = 1.0f;
    public float tt = 2.0f;
    public bool yyy = true;
	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        //每三秒移動
        //一秒移動完
        if (yyy)
        {
            count += Time.deltaTime;
            gameObject.transform.position = Vector3.Lerp(outside.transform.position, inside.transform.position, count / t);
            if (count >= t)
            {
                count1 += Time.deltaTime;
                if(count1 >= tt)
                {
                    count = 0.0f;
                    count1 = 0.0f;
                    yyy = !yyy;
                }
            }
        }
        else
        {
            count += Time.deltaTime;
            gameObject.transform.position = Vector3.Lerp(inside.transform.position, outside.transform.position, count / t);
            if (count >= t)
            {
                count1 += Time.deltaTime;
                if (count1 >= tt)
                {
                    count = 0.0f;
                    count1 = 0.0f;
                    yyy = !yyy;
                }
            }
        }
    }
}
