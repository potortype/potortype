﻿using UnityEngine;
using System.Collections;

public class Draw : MonoBehaviour {
    public GameObject occupied;
    public Vector3 pos;

	// Use this for initialization
	void Start () {
        pos = this.gameObject.transform.position;
    }
	
	// Update is called once per frame
	void Update ()
    {
        pos = this.gameObject.transform.position;
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(gameObject.transform.position, 2.0f);
    }
}
