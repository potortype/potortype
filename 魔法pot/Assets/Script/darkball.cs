﻿using UnityEngine;
using System.Collections;

public class darkball : MonoBehaviour {
    public int needpoint;
    public Color o;
	// Use this for initialization
	void Start () {
        needpoint = MagicDamHandle._Instance.darkpoint;
        o = gameObject.GetComponent<Renderer>().material.color;
    }
	
	// Update is called once per frame
	void Update () {
        if (CameraController._Instance.startPoint >= needpoint)
        {
            Color c = gameObject.GetComponent<Renderer>().material.color;
            c.a = 1.0f;
            gameObject.GetComponent<Renderer>().material.color = c;
        }
        else
        {
            gameObject.GetComponent<Renderer>().material.color = o;
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }
}
