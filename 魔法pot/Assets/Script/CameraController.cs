﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraController : MonoBehaviour
{
    static public CameraController _Instance;
    public enum Playerstate
    {
        OnFloor,
        Seepoint,
        Air,
    }
    public enum Operatingstate
    {
        Idle,
        Touchball,
        holdmagicball,
        spell
    }
    public enum PlayerSpellspecies
    {
        None,
        Thunder,
        directionspell,
        IceWall
    }
    public Playerstate playerstate;
    public Operatingstate operatingstate;
    public PlayerSpellspecies spellspecies;

    //Vive空間
    public GameObject vive;
    public float HP;
    //原本是要用來放準心，但好像用不到
    public GameObject HMD;
    //右手物件
    public GameObject RightHand;
    public GameObject LeftHand;
    SteamVR_TrackedObject trackedObj_l;
    SteamVR_TrackedObject trackedObj_r;
    //用來存eason的位置
    Vector3 v;
    //-------------射線相關----------
    //用來打射線的的攝影機，就是VIVE的眼睛
    public Camera cameronfloor;
    //RAY
    Ray landRayPoint;
    Ray landRayWall;
    //Ray landRayTarget;
    //有沒有打到
    bool prehit;
    //bool prehitTarget;
    bool prehitWall;
    //回傳打到資料的RAY
    RaycastHit RHPoint;
    RaycastHit RHWall;
    RaycastHit RHLine;
    //RaycastHit RHTarget;
    //射線長度
    public float raycastlongth = 100.0f;
    //用來計算看到多久時間
    //float count = 0.0f;
    //人物的Avatar
    public GameObject Eason;
    //放準新的canvas
    //public Canvas see;
    //瞄準點prefab(會生成RaycastHit打在Point上的那個位置)
    public GameObject balls;
    //動態生成的瞄準點物件
    public GameObject ball;
    //o是完全透明，FPS狀態下看不到point
    public Color o;
    //a是半透明，靈魂模式下看到的point
    public Color a;
    //按下trugger變ture，放開變false;
    //public bool press = false;
    //用來存你上一個看到的掩體是哪一個
    public GameObject sawpoint = null;
    //public GameObject sss = null;
    //寫死的物件
    public GameObject wall;
    public GameObject Target;
    public GameObject Table;
    //黏在手上的法術球，之後應該會變成法術，而不是球
    public GameObject newmagicaspell;
    //public GameObject createdaimplane;
    //黏在手上的能量波
    public GameObject energywave;
    //實體飛出去的指向法術( 不包含 落雷、巨錘、冰牆)
    public GameObject spell;
    //所有法術球prefab陣列
    public GameObject[] Allmagicball;
    //動態生成的亂數法術球排序陣列
    public List<GameObject> AllRandommagicball;
    //法術強弱prefab，目前每個法術只有強跟弱兩種
    public GameObject[] spellfireball;
    public GameObject[] spellwaterball;
    public GameObject[] spelllightball;
    public GameObject[] spelldarkball;
    public GameObject[] spellPhoenix;
    public GameObject[] spellThunder;
    public GameObject[] spellIceWall;
    public GameObject[] spellHammer;
    //能量波動prefab
    public GameObject perfabEnergywave;
    //紀錄碰到的法術球
    public GameObject Touchingball;
    //紀錄使用的法術是在AllRandommagicball的哪一個INDEX
    public int HoldingballIndex = -1;
    //紀錄抓取的法術prefab，用來比對法術prefab的名稱，生成正確的法術
    public GameObject Spells;
    //起始點數5(inspector注意)
    public int startPoint;
    //玩家ID，線性法術上會記錄ID
    public int casterID = 1;
    //點數的計時器1秒加一點
    float pointcount = 0.0f;
    public Canvas pointcanvas;
    //確定法術有生成並扣點
    public bool checkspell = false;
    public bool ShowAim = true;
    //public bool ViveController = false;
    public GameObject UIs;
    
    //用來算法術的方向兩個點
    //public Vector3 releasepoint;
    //public Vector3 Hitpoint;
    //public GameObject touchingaimplane;
    //動態法術目標
    public bool UIRay = false;
    public GameObject target2;
    //public List<GameObject> aimplanes;
    //public GameObject aimplaneM;
    //public GameObject Icewall;
    public bool immediately = true;
    public float f_turnoffUIangle = 45.0f;
    public float f_airhight = 10.0f;
    public bool UIonhand = true;
    void Awake()
    {
        _Instance = this;
        //aimplanes = new List<GameObject>();
        //setaimplane();
        AllRandommagicball = new List<GameObject>();
        Shuffle(Allmagicball, AllRandommagicball);
    }
    void Start()
    {
        operatingstate = Operatingstate.Idle;
        playerstate = Playerstate.OnFloor;
        spellspecies = PlayerSpellspecies.None;
        trackedObj_r = RightHand.GetComponent<SteamVR_TrackedObject>();
        trackedObj_l = LeftHand.GetComponent<SteamVR_TrackedObject>();
        HP = GameRunHandle._Instance.PlayerHP;
        //先把透明色存起來
<<<<<<< HEAD:魔法pot/Assets/Script/CameraController.cs
        o = GameRunHandle._Instance.AllPoints[0].GetComponent<Renderer>().material.color;
=======
        o = pointhandle._Instance.allpoint[0].GetComponent<Renderer>().material.color;
>>>>>>> 1380bf0b5e6813af6f0eac79b71600b53291405b:魔法pot/Assets/Script/CameraController.cs

    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("玩家狀態" + playerstate);

        OnOffUIs();
        //頭顯打射線，條件式打到Point，prehit才是true;
        Ray landRayLine = new Ray(cameronfloor.transform.position, cameronfloor.transform.forward);
        bool prehitLine = Physics.Raycast(landRayLine, out RHLine, raycastlongth);

        if (prehitLine)
        {
            Debug.DrawLine(HMD.transform.position, RHLine.point, Color.red);
        }
        //更新點數，
        UpdatePoint();
        //更新五個魔法球的位置
        if (UIonhand)
        {
            updatemagicaroll(LeftHand);
        }
        else
        {
            updatemagicaroll(HMD);
        }

        //更新右手拿著的魔法球位置
        UpdateNewmagicaspellandNewEnergywave();
        //更新aimplane的位置
        //Upadteaimplane();
        //舊的操作模式
        //old();

        //新的
        //一直拿avatar的位置
        v = Eason.transform.position;
        //拿這個位置是要算靈魂模式的高度
        Vector3 Pos = vive.transform.position;
        //獲取兩個控制器

        if (trackedObj_r.index == SteamVR_TrackedObject.EIndex.None)
        {
            return;
        }
        if (trackedObj_l.index == SteamVR_TrackedObject.EIndex.None)
        {
            return;
        }
        var device_r = SteamVR_Controller.Input((int)trackedObj_r.index);
        var device_l = SteamVR_Controller.Input((int)trackedObj_l.index);


        //頭顯打射線，條件式打到Point，prehit才是true;
        landRayPoint = new Ray(cameronfloor.transform.position, cameronfloor.transform.forward);
        prehit = Physics.Raycast(landRayPoint, out RHPoint, raycastlongth, 1 << LayerMask.NameToLayer("Point"));

        if (playerstate == Playerstate.OnFloor)
        {
            //一樣要紀錄並且判斷point
            if (prehit)
            {
                if (ShowAim)
                {
                    if (ball == null)
                    {
                        ball = Instantiate(balls);
                    }
                    if (ball != null)
                    {
                        ball.transform.position = RHWall.point;
                        ball.GetComponent<Renderer>().material.color = Color.green;
                    }
                }
                else
                {
                    if (ball != null)
                    {
                        Destroy(ball);
                        ball = null;
                    }
                }

                //if (sawpoint == null || sawpoint.name != RH.collider.gameObject.name)
                //{
                if (sawpoint == null)
                {
                    Debug.Log("sawpoint = null");
                }
                else
                {
                    Debug.Log("sawpoint = " + sawpoint);
                }
                Debug.Log("設限打到的point = " + RHPoint.collider.gameObject.name);
                //看到的point變半透明
                Color a = RHPoint.collider.gameObject.GetComponent<Renderer>().material.color;
                a.a = 0.2f;
                RHPoint.collider.gameObject.GetComponent<Renderer>().material.color = a;
                //ball.GetComponent<Renderer>().material.color = new Color(1.0f, 0.0f, 0.0f);
                playerstate = Playerstate.Seepoint;
            }
            else
            {
                //count = 0.0f;
                //看到不是點的
                playerstate = Playerstate.OnFloor;
                Debug.Log("GG");
                if (ball != null)
                {
                    Destroy(ball);
                    ball = null;
                }
                //把所有point變成透明
                for (int i = 0; i < GameRunHandle._Instance.AllPoints.Count; i++)
                {

                    GameRunHandle._Instance.AllPoints[i].GetComponent<Renderer>().material.color = o;
                }

            }
        }
        if (playerstate == Playerstate.Seepoint)
        {
            //因為現在掩體的point非常大，所以一定會看到掩體才會看到目標或是牆壁，之後掩體改變可能就要換地方
            Ray landRay = new Ray(cameronfloor.transform.position, cameronfloor.transform.forward);
            bool prehitWall = Physics.Raycast(landRay, out RHWall, raycastlongth, 1 << LayerMask.NameToLayer("Wall") | 1 << LayerMask.NameToLayer("Target") /*| 1 << LayerMask.NameToLayer("Terrain")*/);

            if (prehitWall)
            {

                Debug.DrawLine(cameronfloor.transform.position, RHWall.point, Color.red);

                if (ShowAim)
                {
                    if (ball == null)
                    {
                        ball = Instantiate(balls);
                    }
                    if (ball != null)
                    {
                        ball.transform.position = RHWall.point;
                        ball.GetComponent<Renderer>().material.color = Color.green;
                    }
                }
                else
                {
                    if (ball != null)
                    {
                        Destroy(ball);
                        ball = null;
                    }
                }
                if (wall != null && RHWall.collider.name.Equals("Wall1"))
                {
                    Debug.Log("牆壁變色");
                    //Debug.Log("RH1.collider.gameObject" + RH1.collider.gameObject.name);
                    //wall = RH1.collider.gameObject;
                    wall.GetComponent<Renderer>().material.color = new Color(1.0f, 0.0f, 0.0f);
                    Target.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                    target2 = wall;
                    Debug.Log("目標是 = " + target2);
                }
                else
                {
                    Debug.Log("沒有牆壁");
                    Target.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                }
                if (Target != null && RHWall.collider.name.Equals("Target"))
                {
                    Debug.Log("目標變色");
                    //Debug.Log("RH1.collider.gameObject" + RH1.collider.gameObject.name);
                    //Target = RH1.collider.gameObject;
                    target2 = Target;
                    Debug.Log("目標是 = " + target2);
                    Target.GetComponent<Renderer>().material.color = new Color(1.0f, 0.0f, 0.0f);
                    wall.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                }
                else
                {
                    Debug.Log("沒有目標");
                }
                if (RHWall.collider.name.Equals("G2"))
                {
                    //if (ShowRay)
                    //{
                    //    Debug.DrawLine(cameronfloor.transform.position, RHWall.point, Color.green);
                    //}
                    Debug.Log("打到地形");
                    wall.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                    Target.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                    target2 = null;

                    if (ball != null)
                    {
                        ball.transform.position = RHWall.point;
                        ball.GetComponent<Renderer>().material.color = Color.red;
                    }
                }
                //prehitTarget = false;
            }
            else if (!prehitWall)
            {


                Debug.Log("牆壁變回來");
                wall.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                Target.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                target2 = null;
                if (ball != null)
                {
                    Destroy(ball);
                    ball = null;
                }
            }

            if (prehit)
            {
                //按下trigger切換靈魂
                if ((device_l.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))/* && !press*/)
                {
                    Fader.Instance.FadeOutAndIn(() =>
                    {
                        Debug.Log("GG");
                        //Destroy(ball);
                        //ball = null;
                        //打開avatar
                        Eason.SetActive(true);
                        //Debug.Log("Eason開啟");
                        //設定靈魂模式的高度
                        Pos.y += f_airhight;
                        vive.transform.position = Pos;
                        Debug.Log("I'm faded out. I can do something now before fading back in");
                    });

                    for (int i = 0; i < GameRunHandle._Instance.AllPoints.Count; i++)
                    {
                        if (GameRunHandle._Instance.AllPoints[i].name == RHPoint.collider.gameObject.name)
                        {
                            //press = true;
                            //找看到的point的位置
                            Vector3 pos = GameRunHandle._Instance.AllPoints[i].transform.position;
                            //設定人物的目標地，更換人物狀態?
                            Eason.GetComponent<Easongo>().target = pos;
                            Eason.GetComponent<Easongo>().from = Eason.gameObject.transform.position;
                            Eason.GetComponent<Easongo>().go = true;
                            //因為現在是用內差，每次換目標內差的秒數都歸零
                            Eason.GetComponent<Easongo>().count = 0.0f;
                            Eason.GetComponent<Easongo>().arrival = false;
                            //設定我上一次看到的point是哪一個point
                            sawpoint = GameRunHandle._Instance.AllPoints[i].gameObject;
                            playerstate = Playerstate.Air;
<<<<<<< HEAD:魔法pot/Assets/Script/CameraController.cs
                            Debug.Log("設定上一個sawpoint = " + GameRunHandle._Instance.AllPoints[i].name);
=======
                            Debug.Log("設定上一個sawpoint = " + pointhandle._Instance.allpoint[i].name);
>>>>>>> 1380bf0b5e6813af6f0eac79b71600b53291405b:魔法pot/Assets/Script/CameraController.cs

                        }
                        //不是我要的名稱就找下一個point
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            else
            {
                //count = 0.0f;
                //看到不是點的
                playerstate = Playerstate.OnFloor;
                Debug.Log("GG");
                if (ball != null)
                {
                    Destroy(ball);
                    ball = null;
                }
                //把所有point變成透明
                for (int i = 0; i < GameRunHandle._Instance.AllPoints.Count; i++)
                {

                    GameRunHandle._Instance.AllPoints[i].GetComponent<Renderer>().material.color = o;
                }
            }
        }
        if (playerstate == Playerstate.Air)
        {
            if ((device_l.GetPressUp(SteamVR_Controller.ButtonMask.Trigger)))
            {
                //press = false;

                goback();
                playerstate = Playerstate.OnFloor;

            }
        }
        if (operatingstate == Operatingstate.Idle)
        {
            setIdlestate();
        }

        if (operatingstate == Operatingstate.Touchball)
        {
            if ((device_r.GetPressDown(SteamVR_Controller.ButtonMask.Trigger)))
            {
                NewmagicaspellandNewEnergywave();
                HoldingballIndex = FindIndex();
                operatingstate = Operatingstate.holdmagicball;
                Debug.Log("玩家狀態切換到 = " + operatingstate);
                //生成魔法球
                //進到holdingball
            }
            if ((device_r.GetPressUp(SteamVR_Controller.ButtonMask.Trigger)))
            {
                operatingstate = Operatingstate.Idle;
            }
        }
        if (operatingstate == Operatingstate.holdmagicball)
        {


            //count = 0.0f;
            if ((device_r.GetPressUp(SteamVR_Controller.ButtonMask.Trigger)))
            {
                //一定要碰到瞄準版，一定要有目標才能師法
                if (/*touchingaimplane != null && */target2 != null)
                {
                    operatingstate = Operatingstate.spell;
                    Debug.Log("玩家狀態切換到 = " + operatingstate);
                    //生成魔法球
                    //進到holdingball
                }
                else
                {
                    operatingstate = Operatingstate.Idle;
                }

            }


        }

        if (operatingstate == Operatingstate.spell)
        {
            //生成指向技能
            createspell(Spells);

            //確定法術有生成，並為指向技能
            //if (spell != null && spellspecies == PlayerSpellspecies.directionspell)
            //{
            //    //spell當成母物件，三種不同威力的prefab子物件
            //    //GameObject spell = Instantiate(spells);
            //    //把alpha便1，這裡等到有法術模型的時候就可不用了---------
            //    //Color c = spell.GetComponent<Renderer>().sharedMaterial.color;
            //    //c.a = 1.0f;
            //    //----------------------------------------------------
            //    //好像不能用LocalScale這樣設定大小，那只能用energywave.GetComponent<Energywave>().count分區間，開關對應強度的法術
            //    //因為要設定大小，所以設定完才銷毀
            //    //設定魔法的生成的位置，目標位置
            //    spell.transform.position = righthand.transform.position + new Vector3(0.0f, -0.1f, 0.1f);
            //    //文字檔，好debug
            //    spell.GetComponent<spellball>().testmagic = testmagic;

            //    spell.SetActive(true);
            //    //生成位置要調整，手把前方
            //    //spell.GetComponent<spellball>().from = righthand.transform.position + new Vector3(0.0f, 0.05f, 0.10f);
            //    //要碰撞路徑一定要相同，
            //    spell.GetComponent<spellball>().from = touchingaimplane.transform.position;
            //    //法術目標設定為我碰到的aimplane的目標位置
            //    spell.GetComponent<spellball>().target = touchingaimplane.GetComponent<Aimplane>().targetaimplane.transform.position;
            //    //確定點數夠，並有生成法術
            //}
            if (spell != null && spellspecies == PlayerSpellspecies.directionspell && target2 != null)
            {
                trackingspell ts = spell.GetComponent<trackingspell>();
                //spell當成母物件，三種不同威力的prefab子物件
                //GameObject spell = Instantiate(spells);
                //把alpha便1，這裡等到有法術模型的時候就可不用了---------
                //Color c = spell.GetComponent<Renderer>().sharedMaterial.color;
                //c.a = 1.0f;
                //----------------------------------------------------
                //好像不能用LocalScale這樣設定大小，那只能用energywave.GetComponent<Energywave>().count分區間，開關對應強度的法術
                //因為要設定大小，所以設定完才銷毀
                //設定魔法的生成的位置，目標位置
                spell.transform.position = RightHand.transform.position + new Vector3(0.0f, -0.1f, 0.1f);
                ts.currenttarget = target2;
                ts.finaltarget = target2;
                //ts.currentpathmagic = MagicDamHandle._Instance.allRBpathmagic.Count;
                //Debug.Log("ts.currentpathmagic" + ts.currentpathmagic);
                //文字檔，好debug
                //spell.GetComponent<spellball>().testmagic = testmagic;
                //if (MagicDamHandle._Instance.allRBpathmagic.Count == 0)
                //{
                //    ts.currenttarget = touchingaimplane.GetComponent<Aimplane>().targetaimplane;
                //    ts.finaltarget = touchingaimplane.GetComponent<Aimplane>().targetaimplane;
                //    Debug.Log("path上沒有法術，法術目標為人 = " + ts.finaltarget);
                //}
                //else
                //{
                //    for (int i = 0; i < MagicDamHandle._Instance.allRBpathmagic.Count; i++)
                //    {
                //        Debug.Log("path上有法術，當前法術 = " + ts.gameObject.name + "，路徑上法術數量 = " + MagicDamHandle._Instance.allRBpathmagic.Count + "，路徑上法術名稱 = " + MagicDamHandle._Instance.allRBpathmagic[i].name);
                //        Debug.Log("path上有法術 = " + MagicDamHandle._Instance.allRBpathmagic[i].GetComponent<trackingspell>());
                //        if (MagicDamHandle._Instance.allRBpathmagic[i].GetComponent<trackingspell>() != null)
                //        {
                //            if (MagicDamHandle._Instance.allRBpathmagic[i].GetComponent<trackingspell>().CasterID != ts.CasterID)
                //            {
                //                ts.currenttarget = MagicDamHandle._Instance.allRBpathmagic[i];
                //                Debug.Log("path上有敵方法術，" + ts.gameObject.name + "當前seek目標 = " + ts.currenttarget);
                //                ts.finaltarget = touchingaimplane.GetComponent<Aimplane>().targetaimplane;
                //                Debug.Log("path上有敵方法術，" + ts.gameObject.name + "最終seek目標 = " + ts.finaltarget);
                //            }
                //            else { continue; }
                //        }
                //        else
                //        {
                //            Debug.Log("path上有法術，但是找不到script ");
                //        }
                //        //幹這個break是在幹嘛

                //        break;
                //    }
                //}
                spell.SetActive(true);
                //生成位置要調整，手把前方
                //spell.GetComponent<spellball>().from = righthand.transform.position + new Vector3(0.0f, 0.05f, 0.10f);
                //要碰撞路徑一定要相同，
                //spell.GetComponent<spellball>().from = touchingaimplane.transform.position;
                //法術目標設定為我碰到的aimplane的目標位置

                //確定點數夠，並有生成法術
            }
            else if (spell == null)
            {
                Debug.Log("沒有實體法術");
                Debug.Log("spell" + spell);
            }
            else if (target2 == null)
            {
                Debug.Log("沒有目標");
                Debug.Log("target2" + target2);
            }
            if (spellspecies == PlayerSpellspecies.Thunder)
            {
                //特效之類的
            }
            if (spellspecies == PlayerSpellspecies.IceWall)
            {
                //Icewall.SetActive(true);

                //HP += Icewall.GetComponent<spellball>().dam;
                //Destroy(newmagicaspell);
                //Destroy(energywave);
                //不要讓人物回到IDLE，保持IceWalling;
                //return;
            }
            Destroy(newmagicaspell);
            Destroy(energywave);
            operatingstate = Operatingstate.Idle;
            Debug.Log("玩家狀態切換到 = " + operatingstate);
        }


    }


    public void goback()
    {

        Eason.SetActive(false);

        Fader.Instance.FadeOutAndIn(() =>
        {
            if (ball != null)
            {
                Destroy(ball);
                ball = null;
            }
            Eason.GetComponent<Easongo>().arrival = true;
            Eason.GetComponent<Easongo>().go = false;
            //到人物的位置
            vive.transform.position = v;
            //關閉人物
            playerstate = Playerstate.OnFloor;

            Debug.Log("I'm faded out. I can do something now before fading back in");
        });
        //把所有point變成透明
        //for (int i = 0; i < pointhandle._Instance.allpoint.Length; i++)
        //{

        //    pointhandle._Instance.allpoint[i].GetComponent<Renderer>().material.color = o;
        //}
    }
    //void old()
    //{
    //    //一直拿avatar的位置
    //    v = Eason.transform.position;
    //    //拿這個位置是要算靈魂模式的高度
    //    Vector3 Pos = vive.transform.position;


    //    if (trackedObj_r.index == SteamVR_TrackedObject.EIndex.None)
    //    {
    //        return;
    //    }
    //    var device = SteamVR_Controller.Input((int)trackedObj_r.index);
    //    //按下trugger變成透明模式
    //    if ((device.GetPress(SteamVR_Controller.ButtonMask.Trigger)) && !press)
    //    {
    //        //回復point半透明
    //        for (int i = 0; i < pointhandle._Instance.allpoint.Length; i++)
    //        {
    //            //Debug.Log("恢復顏色");
    //            pointhandle._Instance.allpoint[i].GetComponent<Renderer>().material.color = a;
    //        }
    //        //打開avatar
    //        Eason.SetActive(true);
    //        //Debug.Log("Eason開啟");
    //        //設定靈魂模式的高度
    //        Pos.y = 10.0f;
    //        vive.transform.position = Pos;

    //        //靈魂模式下才打射線，條件式打到Point，prehit才是true;
    //        landRay = new Ray(cameronfloor.transform.position, cameronfloor.transform.forward);
    //        prehit = Physics.Raycast(landRay, out RH, raycastlongth, 1 << LayerMask.NameToLayer("Point"));
    //        //Debug.Log("掃描");
    //        //Debug.Log("sawpoint.name = " + sawpoint.name);
    //        if (prehit)
    //        {
    //            //只要打到的不是上一次看到的point或是根本沒有東西的時候，就可以更換目標，==null是因為第一次進遊戲sawpoint一定是null
    //            //sawpoint.name != RH.collider.gameObject.name 是同一個point不能進行移動，一定要視不同的point
    //            if (sawpoint == null || sawpoint.name != RH.collider.gameObject.name)
    //            {
    //                //計算時間
    //                count += Time.deltaTime;
    //                //在point上生成瞄準點，並更新位置
    //                if (ball == null)
    //                {
    //                    ball = Instantiate(balls);
    //                }
    //                ball.transform.position = RH.point;

    //                //Debug.Log("RH.collider.gameObject.name = " + RH.collider.gameObject.name);
    //                //只要時間還沒到，瞄準點都是紅色的
    //                ball.GetComponent<Renderer>().material.color = new Color(1.0f, 0.0f, 0.0f);
    //                //瞄準時間大於1秒
    //                if (count >= 1.0f)
    //                {
    //                    //大於一秒，瞄準點變成綠色
    //                    ball.GetComponent<Renderer>().material.color = new Color(0.0f, 1.0f, 0.0f);
    //                    //計時歸零
    //                    count = 0.0f;
    //                    //去搜尋我看到的point是全部point的哪一個point
    //                    for (int i = 0; i < pointhandle._Instance.allpoint.Length; i++)
    //                    {
    //                        if (pointhandle._Instance.allpoint[i].name == RH.collider.gameObject.name)
    //                        {
    //                            press = true;
    //                            //找看到的point的位置
    //                            Vector3 pos = pointhandle._Instance.allpoint[i].pos;
    //                            //設定人物的目標地，更換人物狀態?
    //                            Eason.GetComponent<Easongo>().target = pos;
    //                            Eason.GetComponent<Easongo>().from = Eason.gameObject.transform.position;
    //                            Eason.GetComponent<Easongo>().go = true;
    //                            //因為現在是用內差，每次換目標內差的秒數都歸零
    //                            Eason.GetComponent<Easongo>().count = 0.0f;
    //                            Eason.GetComponent<Easongo>().arrival = false;
    //                            //設定我上一次看到的point是哪一個point
    //                            sawpoint = pointhandle._Instance.allpoint[i].gameObject;
    //                            // Debug.Log("設定上一個sawpoint = " + pointhandle._Instance.allpoint[i].name);

    //                        }
    //                        //不是我要的名稱就找下一個point
    //                        else
    //                        {
    //                            continue;
    //                        }
    //                    }
    //                }
    //            }

    //        }
    //        else
    //        {
    //            count = 0.0f;
    //            //看到不是點的
    //            Debug.Log("GG");
    //            Destroy(ball);
    //            ball = null;
    //        }

    //    }

    //    if ((device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger)))
    //    {
    //        press = false;
    //        goback();
    //        //see.enabled = false;


    //    }
    //}
    void updatemagicaroll(GameObject anchorObject)
    {
        if (UIonhand)
        {
            //因為table本身有旋轉，forward裁示正確旋轉
            Table.transform.parent = anchorObject.transform;
            Table.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
            Table.transform.Rotate(Vector3.forward, 0.3f);
            //pointcanvas的位置轉向
            pointcanvas.transform.position = Table.transform.position + new Vector3(0.0f, 0.12f, 0.0f);
            //pointcanvas.transform.LookAt(new Vector3(0.0f, 0.0f, 0.0f));
            AllRandommagicball[0].transform.parent = Table.transform;
            AllRandommagicball[0].transform.localPosition = new Vector3(-0.1414f, -0.1414f, 0.0f);
            //AllRandommagicball[0].transform.RotateAround(Table.transform.position, Vector3.up, 5.0f);
            AllRandommagicball[0].GetComponent<BoxCollider>().enabled = true;
            //AllRandommagicball[0].transform.LookAt(MHDTDUIMountPt.transform.position);
            AllRandommagicball[0].SetActive(true);
            AllRandommagicball[3].transform.parent = Table.transform;
            AllRandommagicball[3].transform.localPosition = new Vector3(-0.1414f, 0.1414f, 0.0f);
            AllRandommagicball[3].GetComponent<BoxCollider>().enabled = true;
            //AllRandommagicball[3].transform.LookAt(MHDTDUIMountPt.transform.position);
            AllRandommagicball[3].SetActive(true);
            AllRandommagicball[1].transform.parent = Table.transform;
            AllRandommagicball[1].transform.localPosition = new Vector3(0.1414f, -0.1414f, 0.0f);
            AllRandommagicball[1].GetComponent<BoxCollider>().enabled = true;
            //AllRandommagicball[1].transform.LookAt(MHDTDUIMountPt.transform.position);
            AllRandommagicball[1].SetActive(true);
            AllRandommagicball[2].transform.parent = Table.transform;
            AllRandommagicball[2].transform.localPosition = new Vector3(0.1414f, 0.1414f, 0.0f);
            AllRandommagicball[2].GetComponent<BoxCollider>().enabled = true;
            //AllRandommagicball[2].transform.LookAt(MHDTDUIMountPt.transform.position);
            AllRandommagicball[2].SetActive(true);
            AllRandommagicball[4].transform.parent = Table.transform;
            AllRandommagicball[4].transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
            //永遠關閉第4個碰撞
            AllRandommagicball[4].GetComponent<BoxCollider>().enabled = false;
            //AllRandommagicball[4].transform.LookAt(MHDTDUIMountPt.transform.position);
            AllRandommagicball[4].SetActive(true);
            //發生使用過的球還在世界中，把它關掉
            //AllRandommagicball[5].SetActive(false);
            //AllRandommagicball[6].SetActive(false);
            //AllRandommagicball[7].SetActive(false);
        }
        else
        {
            //因為table本身有旋轉，forward裁示正確旋轉
            Table.transform.parent = anchorObject.transform;
            Table.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
            Table.transform.localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
            //Table.transform.Rotate(Vector3.forward, 0.3f);
            //pointcanvas的位置轉向
            pointcanvas.transform.position = Table.transform.position + new Vector3(0.0f, 0.12f, 0.0f);
            //pointcanvas.transform.LookAt(new Vector3(0.0f, 0.0f, 0.0f));
            AllRandommagicball[0].transform.parent = Table.transform;
            AllRandommagicball[0].transform.localPosition = new Vector3(-0.35f, -0.2f, 0.5f);
            //AllRandommagicball[0].transform.RotateAround(Table.transform.position, Vector3.up, 5.0f);
            AllRandommagicball[0].GetComponent<BoxCollider>().enabled = true;
            //AllRandommagicball[0].transform.LookAt(MHDTDUIMountPt.transform.position);
            AllRandommagicball[0].SetActive(true);
            AllRandommagicball[3].transform.parent = Table.transform;
            AllRandommagicball[3].transform.localPosition = new Vector3(0.35f, -0.2f, 0.5f);
            AllRandommagicball[3].GetComponent<BoxCollider>().enabled = true;
            //AllRandommagicball[3].transform.LookAt(MHDTDUIMountPt.transform.position);
            AllRandommagicball[3].SetActive(true);
            AllRandommagicball[1].transform.parent = Table.transform;
            AllRandommagicball[1].transform.localPosition = new Vector3(-0.35f, 0.2f, 0.5f);
            AllRandommagicball[1].GetComponent<BoxCollider>().enabled = true;
            //AllRandommagicball[1].transform.LookAt(MHDTDUIMountPt.transform.position);
            AllRandommagicball[1].SetActive(true);
            AllRandommagicball[2].transform.parent = Table.transform;
            AllRandommagicball[2].transform.localPosition = new Vector3(0.35f, 0.2f, 0.5f);
            AllRandommagicball[2].GetComponent<BoxCollider>().enabled = true;
            //AllRandommagicball[2].transform.LookAt(MHDTDUIMountPt.transform.position);
            AllRandommagicball[2].SetActive(true);
            //AllRandommagicball[4].transform.parent = Table.transform;
            //AllRandommagicball[4].transform.localPosition = new Vector3(0.0f, 0.0f, 0.5f);
            ////永遠關閉第4個碰撞
            //AllRandommagicball[4].GetComponent<BoxCollider>().enabled = false;
            ////AllRandommagicball[4].transform.LookAt(MHDTDUIMountPt.transform.position);
            //AllRandommagicball[4].SetActive(true);
            //發生使用過的球還在世界中，把它關掉
            //AllRandommagicball[5].SetActive(false);
            //AllRandommagicball[6].SetActive(false);
            //AllRandommagicball[7].SetActive(false);
        }

    }
    void NewmagicaspellandNewEnergywave()
    {
        //我碰到哪一個球，就生成哪一個球
        newmagicaspell = Instantiate(Touchingball);
        //我碰到哪一個球，就生成哪一個球的能量波，那我要知道我拿的球的RGB設定給能量波
        energywave = Instantiate(perfabEnergywave);
        //我抓哪一個spells，切換energywave的狀態
        //火球
        if (Spells.name == spellfireball[0].name)
        {
            energywave.GetComponent<Energywave>().spellspecies = Energywave.Spellspecies.SetFireBall;
            Debug.Log("火球");
        }
        //冰茅
        if (Spells.name == spellwaterball[0].name)
        {
            energywave.GetComponent<Energywave>().spellspecies = Energywave.Spellspecies.SetIceSpear;
            Debug.Log("冰茅");
        }
        //鳳凰
        if (Spells.name == spellPhoenix[0].name)
        {
            //spell = Instantiate(spellPhoenix[0]);
            energywave.GetComponent<Energywave>().spellspecies = Energywave.Spellspecies.SetPhoenix;
            Debug.Log("鳳凰");
        }
        //冰牆
        if (Spells.name == spellIceWall[0].name)
        {
            //spell = Instantiate(spellIceWall[0]);
            energywave.GetComponent<Energywave>().spellspecies = Energywave.Spellspecies.SetIceWall;
            Debug.Log("冰牆");
        }
        //落雷
        if (Spells.name == spellThunder[0].name)
        {
            //spell = Instantiate(spellThunder[0]);
            energywave.GetComponent<Energywave>().spellspecies = Energywave.Spellspecies.SetThunder;
            Debug.Log("落雷");
        }
        //巨槌
        if (Spells.name == spellHammer[0].name)
        {
            //spell = Instantiate(spellHammer[0]);
            energywave.GetComponent<Energywave>().spellspecies = Energywave.Spellspecies.SetHammer;
            Debug.Log("巨槌");
        }
        //光球
        if (Spells.name == spelllightball[0].name)
        {
            energywave.GetComponent<Energywave>().spellspecies = Energywave.Spellspecies.SetLightBall;
            Debug.Log("光球");
        }
        //暗球
        if (Spells.name == spelldarkball[0].name)
        {
            energywave.GetComponent<Energywave>().spellspecies = Energywave.Spellspecies.SetDarkBall;
            Debug.Log("暗球");
        }

        if (Spells != null)
        {
            //能量波的顏色，是用spells的RGB
            energywave.GetComponent<Energywave>().r = Spells.GetComponent<Renderer>().sharedMaterial.color.r;
            energywave.GetComponent<Energywave>().g = Spells.GetComponent<Renderer>().sharedMaterial.color.g;
            energywave.GetComponent<Energywave>().b = Spells.GetComponent<Renderer>().sharedMaterial.color.b;

            float r = Spells.GetComponent<Renderer>().sharedMaterial.color.r;
            float g = Spells.GetComponent<Renderer>().sharedMaterial.color.g;
            float b = Spells.GetComponent<Renderer>().sharedMaterial.color.b;
            //Debug.Log("rgb" + r + g + b);

        }
        else
        {
            Debug.Log("spells = null");
        }
        if (energywave != null)
        {
            return;
        }
        else
        {
            Debug.Log("energywave = null");
        }
        if (newmagicaspell != null)
        {
            return;
        }
        else
        {
            Debug.Log("newmagicaspell = null");
        }
        //這裡就把玩家使用的法術丟給
        //AiSystem._Instance.playergetwhatspell = Spells;
        Debug.Log("玩家拿起 " + Spells);
    }
    void UpdateNewmagicaspellandNewEnergywave()
    {
        if (newmagicaspell != null)
        {
            //先同步看在哪
            newmagicaspell.transform.parent = RightHand.transform;
            newmagicaspell.transform.localPosition = new Vector3(0.0f, -0.1f, 0.1f);
            energywave.transform.parent = RightHand.transform;
            energywave.transform.localPosition = new Vector3(0.0f, -0.1f, 0.1f);
        }
        else
        {
            // Debug.Log("newmagicaspell = null");
            return;
        }
    }
    void createspell(GameObject spells)
    {
        //拿放開trigger的位置 - 進去aimplane的位置
        //Vector3 spelldir = (releasepoint - Hitpoint).normalized;
        int needpoint;
        //先看拿的是甚麼法術，再判斷時間，這裡寫得好爛，既然都有記錄我拿的法術了，幹嘛還要再去判斷明子
        //if (spells.name == spellfireball[0].name)
        //{
        //    needpoint = MagicDamHandle._Instance.firepoint;
        //    //點數夠的話，才升成法術
        //    if (startPoint >= needpoint)
        //    {
        //        if (energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.normalfireballtime)
        //        {
        //            spell = Instantiate(spellfireball[0]);
        //            spell.GetComponent<spellball>().dam = MagicDamHandle._Instance.fireballSdam;

        //        }
        //        else if (MagicDamHandle._Instance.normalfireballtime < energywave.GetComponent<Energywave>().count) /*&& energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.plusfireballtime)*/
        //        {
        //            spell = Instantiate(spellfireball[1]);
        //            spell.GetComponent<spellball>().dam = MagicDamHandle._Instance.fireballLdam;
        //        }
        //        spell.GetComponent<spellball>().spellspecies = spellball.Spellspecies.Directional;
        //        startPoint -= needpoint;
        //        checkspell = true;
        //        spellspecies = PlayerSpellspecies.directionspell;
        //        spell.GetComponent<spellball>().CasterID = casterID;
        //        Debug.Log("火球施術者 = " + casterID);
        //    }
        //    else
        //    {
        //        state = playerstate.Idle;
        //        checkspell = false;
        //        Debug.Log("點數不夠，無法施法火球，回到IDLE");
        //    }

        //}
        if (spells.name == spellfireball[0].name)
        {
            needpoint = MagicDamHandle._Instance.firepoint;
            //點數夠的話，才升成法術
            if (startPoint >= needpoint)
            {
                if (energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.normalfireballtime)
                {
                    spell = Instantiate(spellfireball[0]);
                    spell.GetComponent<trackingspell>().dam = MagicDamHandle._Instance.fireballSdam;
                }
                else if (MagicDamHandle._Instance.normalfireballtime < energywave.GetComponent<Energywave>().count) /*&& energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.plusfireballtime)*/
                {
                    spell = Instantiate(spellfireball[1]);
                    spell.GetComponent<trackingspell>().dam = MagicDamHandle._Instance.fireballLdam;
                }
                spell.GetComponent<trackingspell>().currentspeed = MagicDamHandle._Instance.spellspeed;
                spell.transform.forward = HMD.transform.forward;
                startPoint -= needpoint;
                checkspell = true;
                spellspecies = PlayerSpellspecies.directionspell;
                spell.GetComponent<trackingspell>().CasterID = casterID;
                Debug.Log("火球消耗 = " + needpoint);
                Debug.Log("火球施術者 = " + casterID);
            }
            else
            {
                operatingstate = Operatingstate.Idle;
                checkspell = false;
                Debug.Log("點數不夠，無法施法火球，回到IDLE");
            }

        }
        if (spells.name == spellwaterball[0].name)
        {
            needpoint = MagicDamHandle._Instance.waterpoint;
            //點數夠的話，才升成法術
            if (startPoint >= needpoint)
            {
                if (energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.normalwaterballtime)
                {
                    spell = Instantiate(spellwaterball[0]);
                    spell.GetComponent<trackingspell>().dam = MagicDamHandle._Instance.waterballSdam;

                }
                else if (MagicDamHandle._Instance.normalwaterballtime < energywave.GetComponent<Energywave>().count)/* && energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.pluswaterballtime)*/
                {
                    spell = Instantiate(spellwaterball[1]);
                    spell.GetComponent<trackingspell>().dam = MagicDamHandle._Instance.waterballLdam;
                }
                spell.GetComponent<trackingspell>().currentspeed = MagicDamHandle._Instance.spellspeed;
                spell.transform.forward = HMD.transform.forward;
                startPoint -= needpoint;
                checkspell = true;
                spellspecies = PlayerSpellspecies.directionspell;

                spell.GetComponent<trackingspell>().CasterID = casterID;
                Debug.Log("冰毛消耗 = " + needpoint);
                Debug.Log("冰茅施術者 = " + casterID);
            }
            else
            {
                operatingstate = Operatingstate.Idle;
                checkspell = false;
                Debug.Log("點數不夠，無法施法冰茅，回到IDLE");
            }
        }
        if (spells.name == spelllightball[0].name)
        {
            needpoint = MagicDamHandle._Instance.lightpoint;
            //點數夠的話，才升成法術
            if (startPoint >= needpoint)
            {
                if (energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.normallightballtime)
                {
                    spell = Instantiate(spelllightball[0]);
                    spell.GetComponent<trackingspell>().dam = MagicDamHandle._Instance.lightballSdam;

                }
                else if (MagicDamHandle._Instance.normallightballtime < energywave.GetComponent<Energywave>().count)/* && energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.pluslightballtime)*/
                {
                    spell = Instantiate(spelllightball[1]);
                    spell.GetComponent<trackingspell>().dam = MagicDamHandle._Instance.lightballLdam;
                }
                spell.GetComponent<trackingspell>().currentspeed = MagicDamHandle._Instance.spellspeed;
                spell.transform.forward = HMD.transform.forward;
                startPoint -= needpoint;
                checkspell = true;
                spellspecies = PlayerSpellspecies.directionspell;
                spell.GetComponent<trackingspell>().CasterID = casterID;
                Debug.Log("光球消耗 = " + needpoint);
                Debug.Log("光球施術者 = " + casterID);
            }
            else
            {
                operatingstate = Operatingstate.Idle;
                checkspell = false;
                Debug.Log("點數不夠，無法施法光球，回到IDLE");

            }
        }
        if (spells.name == spelldarkball[0].name)
        {
            needpoint = MagicDamHandle._Instance.darkpoint;
            //點數夠的話，才升成法術
            if (startPoint >= needpoint)
            {
                if (energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.normaldarkballtime)
                {
                    spell = Instantiate(spelldarkball[0]);
                    spell.GetComponent<trackingspell>().dam = MagicDamHandle._Instance.darkballSdam;
                }
                else if (MagicDamHandle._Instance.normaldarkballtime < energywave.GetComponent<Energywave>().count)/* && energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.plusdarkballtime)*/
                {
                    spell = Instantiate(spelldarkball[1]);
                    spell.GetComponent<trackingspell>().dam = MagicDamHandle._Instance.darkballLdam;
                }
                spell.GetComponent<trackingspell>().currentspeed = MagicDamHandle._Instance.spellspeed;
                spell.transform.forward = HMD.transform.forward;
                startPoint -= needpoint;
                checkspell = true;
                spellspecies = PlayerSpellspecies.directionspell;
                spell.GetComponent<trackingspell>().CasterID = casterID;
                Debug.Log("案求消耗 = " + needpoint);
                Debug.Log("暗球施術者 = " + casterID);
            }
            else
            {
                operatingstate = Operatingstate.Idle;
                checkspell = false;
                Debug.Log("點數不夠，無法施法暗球，回到IDLE");
            }
        }
        //鳳凰
        if (spells.name == spellPhoenix[0].name)
        {
            needpoint = MagicDamHandle._Instance.Phoenixpoint;
            //點數夠的話，才升成法術
            if (startPoint >= needpoint)
            {
                if (energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.normalPhoenixtime)
                {
                    spell = Instantiate(spellPhoenix[0]);
                    spell.GetComponent<trackingspell>().dam = MagicDamHandle._Instance.PhoenixSdam;
                }
                else if (MagicDamHandle._Instance.normalPhoenixtime < energywave.GetComponent<Energywave>().count)/* && energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.plusPhoenixtime)*/
                {
                    spell = Instantiate(spellPhoenix[1]);
                    spell.GetComponent<trackingspell>().dam = MagicDamHandle._Instance.PhoenixLdam;
                }
                spell.GetComponent<trackingspell>().currentspeed = MagicDamHandle._Instance.spellspeed;
                spell.transform.forward = HMD.transform.forward;
                startPoint -= needpoint;
                checkspell = true;
                spellspecies = PlayerSpellspecies.directionspell;

                spell.GetComponent<trackingspell>().CasterID = casterID;
                Debug.Log("火球消耗 = " + needpoint);
                Debug.Log("暗球施術者 = " + casterID);
            }
            else
            {
                operatingstate = Operatingstate.Idle;
                checkspell = false;
                Debug.Log("點數不夠，無法施法暗球，回到IDLE");
            }
        }
        ////冰牆
        //if (spells.name == spellIceWall[0].name)
        //{
        //    needpoint = MagicDamHandle._Instance.IceWallpoint;
        //    //點數夠的話，才升成法術
        //    if (startPoint >= needpoint)
        //    {
        //        if (energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.normalIceWalltime)
        //        {
        //            if (Icewall != null)
        //            {
        //                Debug.Log("已經有冰牆，先銷毀");
        //                Destroy(Icewall);
        //            }
        //            Icewall = Instantiate(spellIceWall[0]);
        //            Icewall.transform.position = MHDTDUIMountPt.transform.position + new Vector3(0.0f, 0.0f, 1.0f);
        //            Icewall.GetComponent<spellball>().dam = MagicDamHandle._Instance.IceWallSdam;
        //        }
        //        else if (MagicDamHandle._Instance.normalIceWalltime < energywave.GetComponent<Energywave>().count)/* && energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.plusIceWalltime)*/
        //        {
        //            if (Icewall != null)
        //            {
        //                Debug.Log("已經有冰牆，先銷毀");
        //                Destroy(Icewall);
        //            }
        //            Icewall = Instantiate(spellIceWall[1]);
        //            Icewall.transform.position = MHDTDUIMountPt.transform.position + new Vector3(0.0f, 0.0f, 1.0f);
        //            Icewall.GetComponent<spellball>().dam = MagicDamHandle._Instance.IceWallLdam;
        //        }
        //        Icewall.GetComponent<spellball>().Caster = MHDTDUIMountPt;
        //        //state = playerstate.IceWalling;
        //        Icewall.GetComponent<spellball>().spellspecies = spellball.Spellspecies.IceWall;
        //        startPoint -= needpoint;
        //        checkspell = true;
        //        spellspecies = PlayerSpellspecies.IceWall;

        //        Icewall.GetComponent<spellball>().CasterID = casterID;
        //        Debug.Log("暗球施術者 = " + casterID);
        //    }
        //    else
        //    {
        //        state = playerstate.Idle;
        //        checkspell = false;
        //        Debug.Log("點數不夠，無法施法暗球，回到IDLE");
        //    }
        //}
        ////巨錘
        //if (spells.name == spellHammer[0].name)
        //{
        //    needpoint = MagicDamHandle._Instance.Hammerpoint;
        //    //點數夠的話，才升成法術
        //    if (startPoint >= needpoint)
        //    {
        //        if (energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.normalHammertime)
        //        {
        //            //spell = Instantiate(spellHammer[0]);
        //            //spell.GetComponent<spellball>().dam = MagicDamHandle._Instance.HammerSdam;
        //            //Debug.Log("巨錘!!!!!!!!!!!" + casterID);
        //            GameRunHandle._Instance.PlayerList[1].GetComponent<AImagica>().aIAbnormalstate = AImagica.AIAbnormalstate.beHammernormal;
        //            //Debug.Log("卡5");
        //        }
        //        else if (MagicDamHandle._Instance.normalHammertime < energywave.GetComponent<Energywave>().count)/* && energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.plusHammertime)*/
        //        {
        //            //spell = Instantiate(spellHammer[1]);
        //            //spell.GetComponent<spellball>().dam = MagicDamHandle._Instance.HammerLdam;
        //            //Debug.Log("巨錘!!!!!!!!!!!" + casterID);
        //            GameRunHandle._Instance.PlayerList[1].GetComponent<AImagica>().aIAbnormalstate = AImagica.AIAbnormalstate.beHammerplus;
        //            //Debug.Log("卡6");

        //        }
        //        //spell.GetComponent<spellball>().spellspecies = spellball.Spellspecies.Hammer;
        //        startPoint -= needpoint;
        //        checkspell = true;
        //        spellspecies = PlayerSpellspecies.Hammer;

        //        //spell.GetComponent<spellball>().CasterID = casterID;
        //        Debug.Log("巨錘施術者 = " + casterID);
        //    }
        //    else
        //    {
        //        state = playerstate.Idle;
        //        checkspell = false;
        //        Debug.Log("點數不夠，無法施法暗球，回到IDLE");
        //    }
        //}
        ////落雷，那就不用生成法術，被打的人會自己生成
        //if (spells.name == spellThunder[0].name)
        //{
        //    needpoint = MagicDamHandle._Instance.Thunderpoint;
        //    //點數夠的話，才升成法術
        //    if (startPoint >= needpoint)
        //    {
        //        if (energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.normalThundertime)
        //        {
        //            //spell = Instantiate(spellThunder[0]);
        //            //去找玩家，強制進入shock，這邊先強制寫，之後可能要改
        //            //Debug.Log("落雷!!!!!!!!!!!" + casterID);
        //            GameRunHandle._Instance.PlayerList[1].GetComponent<AImagica>().aistate = AImagica.AIstate.beshocknormal;
        //            //spell.GetComponent<spellball>().dam = MagicDamHandle._Instance.ThunderSdam;
        //        }
        //        else if (MagicDamHandle._Instance.normalThundertime < energywave.GetComponent<Energywave>().count)/* && energywave.GetComponent<Energywave>().count <= MagicDamHandle._Instance.plusThundertime)*/
        //        {
        //            //spell = Instantiate(spellThunder[1]);
        //            //去找玩家，強制進入shock
        //            //Debug.Log("落雷!!!!!!!!!!!" + casterID);
        //            GameRunHandle._Instance.PlayerList[1].GetComponent<AImagica>().aistate = AImagica.AIstate.beshockplus;
        //            //spell.GetComponent<spellball>().dam = MagicDamHandle._Instance.ThunderLdam;
        //        }
        //        //spell.GetComponent<spellball>().spellspecies = spellball.Spellspecies.Thunder;
        //        startPoint -= needpoint;
        //        checkspell = true;
        //        spellspecies = PlayerSpellspecies.Thunder;
        //        //spell.GetComponent<spellball>().CasterID = casterID;
        //        Debug.Log("落雷施術者 = " + casterID);
        //    }
        //    else
        //    {
        //        state = playerstate.Idle;
        //        checkspell = false;
        //        Debug.Log("點數不夠，無法施法暗球，回到IDLE");
        //    }
        //}
    }
    void UpdatePoint()
    {
        //小於10顆計時
        if (startPoint < GameRunHandle._Instance.maxpoint)
        {
            pointcount += Time.deltaTime;
            //大於1秒，補一顆，並歸零
            if (pointcount >= GameRunHandle._Instance.increacepointtime)
            {
                pointcount = 0.0f;
                startPoint++;
            }
        }
        else
        {
            return;
        }

    }
    public void Shuffle(GameObject[] Source, List<GameObject> Randoms)
    {
        if (Source == null) return;
        int len = Source.Length;

        int r;
        //暫存用
        GameObject tmp;
        //先把物件生成好
        for (int j = 0; j <= len - 1; j++)
        {
            //取亂數後的索引與原來的交換
            tmp = Instantiate(Source[j]);
            string tmps = Source[j].name;
            tmp.name = tmps;
            Randoms.Add(tmp);
            //試試看我加進去後，銷毀原本的之後加進去的LIST會不會也消失
            //經過測試，物件加到LIST後，銷毀物件LIST裡面也會消失
            //Destroy(tmp);
            //Debug.Log("放到第幾個" + j);
        }

        for (int i = 0; i < len - 1; i++)
        {
            //取亂數，範圍包含最小值，不包含最大值
            r = Random.Range(i, len);

            //如果一樣則重取            
            if (i == r) continue;

            //取亂數後的索引與原來的交換
            tmp = Randoms[i];
            Randoms[i] = Randoms[r];
            Randoms[r] = tmp;
        }
        foreach (GameObject g in Randoms)
        {
            //Debug.Log(g.name);
        }

    }
    public int FindIndex()
    {
        string tbn = Touchingball.name;

        for (int i = 0; i < AllRandommagicball.Count; i++)
        {
            if (tbn != AllRandommagicball[i].name)
            {
                continue;
            }
            else
            {
                return i;
            }
        }
        return -2;
    }
    public void UpdateMagicRoll()
    {
        //先把3用removeat，放到最後一個
        GameObject tmp = AllRandommagicball[HoldingballIndex];
        //Destroy(AllRandommagicball[HoldingballIndex]);
        AllRandommagicball.RemoveAt(HoldingballIndex);
        Debug.Log("移除" + HoldingballIndex);
        AllRandommagicball.Add(tmp);
        //驗證用
        //foreach (GameObject g in AllRandommagicball)
        //{
        //    Debug.Log(g.name);
        //}
    }
    //void setaimplane()
    //{
    //GameObject go = Instantiate(aimplaneL);
    //go.GetComponent<Aimplane>().targetaimplane = target1;
    //go.GetComponent<Aimplane>().Owner = this;
    //aimplanes.Add(go);
    //GameObject go1 = Instantiate(aimplaneM);
    //go1.GetComponent<Aimplane>().targetaimplane = target2;
    //go1.GetComponent<Aimplane>().Owner = this;
    //測試用
    //go1.AddComponent<playertest>();
    //aimplanes.Add(go1);
    //GameObject go2 = Instantiate(aimplaneR);
    //go2.GetComponent<Aimplane>().targetaimplane = target3;
    //go2.GetComponent<Aimplane>().Owner = this;
    //aimplanes.Add(go2);
    //}
    //void Upadteaimplane()
    //{

    //aimplanes[0].transform.parent = HMD.transform;
    //aimplanes[0].transform.localPosition = new Vector3(0.0f, 0.0f, 0.5f);
    //aimplanes[0].transform.localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);

    //float x = HMD.transform.position.x;
    //float z = HMD.transform.position.z;

    //Vector3 pos0 = aimplanes[0].GetComponent<Aimplane>().pos;
    //pos0.x = x;
    //pos0.z = z;
    //pos0.y = this.transform.position.y + 1.5f;
    //aimplanes[0].GetComponent<Aimplane>().pos = (pos0 + new Vector3(0.0f, 0.0f, 0.7f));

    //Vector3 pos1 = aimplanes[1].GetComponent<Aimplane>().pos;
    //pos1.x = x;
    //pos1.z = z;
    //pos1.y = this.transform.position.y + 1.5f;
    //aimplanes[1].GetComponent<Aimplane>().pos = (pos1 + new Vector3(0.5f, 0.0f, 0.5f));

    //Vector3 pos2 = aimplanes[2].GetComponent<Aimplane>().pos;
    //pos2.x = x;
    //pos2.z = z;
    //pos2.y = this.transform.position.y + 1.5f;
    //aimplanes[2].GetComponent<Aimplane>().pos = (pos2 + new Vector3(0.7f, 0.0f, 0.0f));
    //}
    void setIdlestate()
    {
        //if (touchingaimplane != null)
        //{
        //    Debug.Log("IDLE狀態，清空touchingaimplane");
        //    touchingaimplane = null;
        //}
        if (newmagicaspell != null)
        {
            Debug.Log("IDLE狀態，銷毀newmagicaball");
            Destroy(newmagicaspell);
        }
        if (Spells != null)
        {
            Debug.Log("IDLE狀態，清空spells");
            Spells = null;
        }
        if (Touchingball != null)
        {
            Debug.Log("IDLE狀態，清空Touchingball");
            Touchingball = null;
        }
        if (energywave != null)
        {
            Debug.Log("IDLE狀態，energywave");
            Destroy(energywave);
        }
        if (HoldingballIndex == -2)
        {
            Debug.Log("找不到該球在List的Index");
            HoldingballIndex = -1;
        }
        else if (HoldingballIndex >= 0 && checkspell)
        {
            //確定法術放出去了，把原本的法術移到最後一個把4號位的移到這個Index
            Debug.Log("checkspellINIdle應該會是true = " + checkspell);

            UpdateMagicRoll();
            Debug.Log("更新Roll");

            Debug.Log("HoldingballIndex初始化");
            HoldingballIndex = -1;
            checkspell = false;
        }
        //打開手把碰撞
        //RightAttachPiont.GetComponent<SphereCollider>().enabled = true;

    }
    void OnOffUIs()
    {
        if (UIRay)
        {
            Debug.DrawLine(HMD.transform.position, HMD.transform.position + HMD.transform.forward * 100.0f, Color.red);
            Debug.DrawLine(HMD.transform.position, HMD.transform.position + -Vector3.up * 100.0f, Color.green);
        }
        float theta = Mathf.Acos(Vector3.Dot(HMD.transform.forward, -Vector3.up));
        float angle = Mathf.Rad2Deg * theta;
        //Debug.Log("攝影機與-UP的夾角 = " + angle);
        if (immediately)
        {
            if (angle <= f_turnoffUIangle)
            {
                //UIs.SetActive(false);
                //Debug.Log("關閉眼前UI");
                //根據角度淡化UI
            }
            else
            {
                //UIs.SetActive(true);
            }
        }
        else
        {
            //根據角度淡入淡出或是甚麼的
        }
       
    }
    //現在有個麻煩如果要照著地行走SWS要怎麼用，SWS用於固定的路徑很強大，但是如果路徑不是固定的話呢
    //還有一定得要走(或是順移)到WP0才能開始路徑，這是一個敗筆，我沒辦法從我最近的WP開始走，最後還是得用ASTAR嗎
    //不然就是要動態生成path要怎麼做?，那還有個問題就是我也要動態去設定watpoint的位置
    //還是所有path都做死，要移動的時候去搜尋最近的point，然後找到他所屬的path
    //可以在Playmode中生成，不過，有幾個問題
    //一，現在都是用滑鼠操控，要怎麼按下start path，還有Enter Path Name的輸入
    //DEMO的作法是把路徑先做成prefab在去instance，其實這也是先做死到時候拿來用

    //總結一下現在的操作模式
    //移動方面
    //  頭顯到到point之後按下左trigger，人物模型是以平滑移動(Lerp)的方式所以會飄在空中，如果能以這樣的移動方式最好這樣我每次移動都是一樣的時間跟距離
    //  但是我有限制就是我不能移動不一樣的距離，因為適用內插所以時間會一樣
    //  如果要用路徑那可能要先做好所有可以走的路徑模型，要用的時候再開用完關閉
    //  使用移動的機制?
    //UI方面
    //  目前是用UI開關的方式可以打開技能實體，再用手去碰
    //鎖定敵人方面
    //  依照超人的意思，是希望有輔助瞄準，看到大概的位置可以鎖定，實際做法要想
    //攻擊方式
    //  針對人的/針對平台
    //輔助方式
    //  針對人/針對平台/隊伍性質
}

