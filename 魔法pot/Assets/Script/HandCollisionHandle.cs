﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HandCollisionHandle : MonoBehaviour {
    //public GameObject RightHand;
    //SteamVR_TrackedObject trackedObj_r;
    // Use this for initialization
    void Start () {
        //trackedObj_r = RightHand.GetComponent<SteamVR_TrackedObject>();
    }
	
	// Update is called once per frame
	void Update () {
        //if (trackedObj_r.index == SteamVR_TrackedObject.EIndex.None)
        //{
        //    return;
        //}
        //var device_r = SteamVR_Controller.Input((int)trackedObj_r.index);
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Equals("SkillsButton"))
        {
            ButtonHandle._Instance.OnPressSkillButton();

        }
        if (collision.gameObject.name.Equals("fireball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Idle)
        {
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Touchball;
            //Debug.Log("玩家狀態切換到 = " + CameraController._Instance.Operatingstate);
        }
        if (collision.gameObject.name.Equals("waterball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Idle)
        {
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Touchball;
            //Debug.Log("玩家狀態切換到 = " + CameraController._Instance.Operatingstate);
        }
        if (collision.gameObject.name.Equals("lightball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Idle)
        {
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Touchball;
            //Debug.Log("玩家狀態切換到 = " + CameraController._Instance.Operatingstate);
        }
        if (collision.gameObject.name.Equals("darkball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Idle)
        {
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Touchball;
            //Debug.Log("玩家狀態切換到 = " + CameraController._Instance.Operatingstate);
        }
        if (collision.gameObject.name.Equals("Phoenix") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Idle)
        {
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Touchball;
            //Debug.Log("玩家狀態切換到 = " + CameraController._Instance.Operatingstate);
        }
        if (collision.gameObject.name.Equals("IceWall") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Idle)
        {
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Touchball;
            //Debug.Log("玩家狀態切換到 = " + CameraController._Instance.Operatingstate);
        }
        if (collision.gameObject.name.Equals("Hammer") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Idle)
        {
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Touchball;
            //Debug.Log("玩家狀態切換到 = " + CameraController._Instance.Operatingstate);
        }
        if (collision.gameObject.name.Equals("Thunder") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Idle)
        {
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Touchball;
            //Debug.Log("玩家狀態切換到 = " + CameraController._Instance.Operatingstate);
        }
        Debug.Log("玩家狀態切換到 = " + CameraController._Instance.operatingstate);

    }
    void OnCollisionStay(Collision collision)
    {
        ////放在Enter有點怪怪的，有時候沒切換，放到Stay試試看
        //if (collision.gameObject.name.Equals("fireball") && CameraController._Instance.Operatingstate == CameraController.playerOperatingstate.Seepoint)
        //{
        //    CameraController._Instance.Operatingstate = CameraController.playerOperatingstate.Touchball;
        //    Debug.Log("玩家狀態切換到 = " + CameraController._Instance.Operatingstate);
        //}
        //if (collision.gameObject.name.Equals("waterball") && CameraController._Instance.Operatingstate == CameraController.playerOperatingstate.Seepoint)
        //{
        //    CameraController._Instance.Operatingstate = CameraController.playerOperatingstate.Touchball;
        //    Debug.Log("玩家狀態切換到 = " + CameraController._Instance.Operatingstate);
        //}
        //if (collision.gameObject.name.Equals("lightball") && CameraController._Instance.Operatingstate == CameraController.playerOperatingstate.Seepoint)
        //{
        //    CameraController._Instance.Operatingstate = CameraController.playerOperatingstate.Touchball;
        //    Debug.Log("玩家狀態切換到 = " + CameraController._Instance.Operatingstate);
        //}
        //if (collision.gameObject.name.Equals("darkball") && CameraController._Instance.Operatingstate == CameraController.playerOperatingstate.Seepoint)
        //{
        //    CameraController._Instance.Operatingstate = CameraController.playerOperatingstate.Touchball;
        //    Debug.Log("玩家狀態切換到 = " + CameraController._Instance.Operatingstate);
        //}
        

        if (collision.gameObject.name.Equals("fireball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            if (CameraController._Instance.Touchingball == null)
                //CameraController._Instance.fireball，之後應該會換成一個靜態不會動的法術prefab
                CameraController._Instance.Touchingball = CameraController._Instance.Allmagicball[0];
            //拿在手上的，統一都是強度最弱的spellfireball[0]
            if (CameraController._Instance.Spells == null)
                CameraController._Instance.Spells = CameraController._Instance.spellfireball[0];
        }
        if (collision.gameObject.name.Equals("waterball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            if (CameraController._Instance.Touchingball == null)
                //CameraController._Instance.fireball，之後應該會換成一個靜態不會動的法術prefab
                CameraController._Instance.Touchingball = CameraController._Instance.Allmagicball[1];
            //拿在手上的，統一都是強度最弱的spellfireball[0]
            if (CameraController._Instance.Spells == null)
                CameraController._Instance.Spells = CameraController._Instance.spellwaterball[0];
        }
        if (collision.gameObject.name.Equals("lightball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            if (CameraController._Instance.Touchingball == null)
                //CameraController._Instance.fireball，之後應該會換成一個靜態不會動的法術prefab
                CameraController._Instance.Touchingball = CameraController._Instance.Allmagicball[2];
            //拿在手上的，統一都是強度最弱的spellfireball[0]
            if (CameraController._Instance.Spells == null)
                CameraController._Instance.Spells = CameraController._Instance.spelllightball[0];
        }
        if (collision.gameObject.name.Equals("darkball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            if (CameraController._Instance.Touchingball == null)
                //CameraController._Instance.fireball，之後應該會換成一個靜態不會動的法術prefab
                CameraController._Instance.Touchingball = CameraController._Instance.Allmagicball[3];
            //拿在手上的，統一都是強度最弱的spellfireball[0]
            if (CameraController._Instance.Spells == null)
                CameraController._Instance.Spells = CameraController._Instance.spelldarkball[0];
        }
        if (collision.gameObject.name.Equals("Phoenix") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            if (CameraController._Instance.Touchingball == null)
                //CameraController._Instance.fireball，之後應該會換成一個靜態不會動的法術prefab
                CameraController._Instance.Touchingball = CameraController._Instance.Allmagicball[4];
            //拿在手上的，統一都是強度最弱的spellfireball[0]
            if (CameraController._Instance.Spells == null)
                CameraController._Instance.Spells = CameraController._Instance.spellPhoenix[0];
        }
        if (collision.gameObject.name.Equals("IceWall") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            if (CameraController._Instance.Touchingball == null)
                //CameraController._Instance.fireball，之後應該會換成一個靜態不會動的法術prefab
                CameraController._Instance.Touchingball = CameraController._Instance.Allmagicball[5];
            //拿在手上的，統一都是強度最弱的spellfireball[0]
            if (CameraController._Instance.Spells == null)
                CameraController._Instance.Spells = CameraController._Instance.spellIceWall[0];
        }
        if (collision.gameObject.name.Equals("Thunder") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            if (CameraController._Instance.Touchingball == null)
                //CameraController._Instance.fireball，之後應該會換成一個靜態不會動的法術prefab
                CameraController._Instance.Touchingball = CameraController._Instance.Allmagicball[6];
            //拿在手上的，統一都是強度最弱的spellfireball[0]
            if (CameraController._Instance.Spells == null)
                CameraController._Instance.Spells = CameraController._Instance.spellThunder[0];
        }
        if (collision.gameObject.name.Equals("Hammer") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            if (CameraController._Instance.Touchingball == null)
                //CameraController._Instance.fireball，之後應該會換成一個靜態不會動的法術prefab
                CameraController._Instance.Touchingball = CameraController._Instance.Allmagicball[7];
            //拿在手上的，統一都是強度最弱的spellfireball[0]
            if (CameraController._Instance.Spells == null)
                CameraController._Instance.Spells = CameraController._Instance.spellHammer[0];
        }
    }
    void OnCollisionExit(Collision collision)
    {
        
        if (collision.gameObject.name.Equals("fireball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.holdmagicball)
        {
            CameraController._Instance.Touchingball = null;
            return;
        }
        if (collision.gameObject.name.Equals("fireball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            CameraController._Instance.Touchingball = null;
            //CameraController._Instance.spells = null;
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Idle;
            Debug.Log("玩家狀態切換到 = " + CameraController._Instance.operatingstate);
        }
        if (collision.gameObject.name.Equals("waterball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.holdmagicball)
        {
            CameraController._Instance.Touchingball = null;
            return;
        }
        if (collision.gameObject.name.Equals("waterball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            CameraController._Instance.Touchingball = null;
            //CameraController._Instance.spells = null;
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Idle;
            Debug.Log("玩家狀態切換到 = " + CameraController._Instance.operatingstate);
        }
        if (collision.gameObject.name.Equals("lightball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.holdmagicball)
        {
            CameraController._Instance.Touchingball = null;
            return;
        }
        if (collision.gameObject.name.Equals("lightball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            CameraController._Instance.Touchingball = null;
            //CameraController._Instance.spells = null;
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Idle;
            Debug.Log("玩家狀態切換到 = " + CameraController._Instance.operatingstate);
        }
        if (collision.gameObject.name.Equals("darkball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.holdmagicball)
        {
            CameraController._Instance.Touchingball = null;
            return;
        }
        if (collision.gameObject.name.Equals("darkball") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            CameraController._Instance.Touchingball = null;
            //CameraController._Instance.spells = null;
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Idle;
            Debug.Log("玩家狀態切換到 = " + CameraController._Instance.operatingstate);
        }
        if (collision.gameObject.name.Equals("Phoenix") && CameraController._Instance.operatingstate == CameraController.Operatingstate.holdmagicball)
        {
            CameraController._Instance.Touchingball = null;
            return;
        }
        if (collision.gameObject.name.Equals("Phoenix") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            CameraController._Instance.Touchingball = null;
            //CameraController._Instance.spells = null;
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Idle;
            Debug.Log("玩家狀態切換到 = " + CameraController._Instance.operatingstate);
        }
        if (collision.gameObject.name.Equals("Hammer") && CameraController._Instance.operatingstate == CameraController.Operatingstate.holdmagicball)
        {
            CameraController._Instance.Touchingball = null;
            return;
        }
        if (collision.gameObject.name.Equals("Hammer") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            CameraController._Instance.Touchingball = null;
            //CameraController._Instance.spells = null;
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Idle;
            Debug.Log("玩家狀態切換到 = " + CameraController._Instance.operatingstate);
        }
        if (collision.gameObject.name.Equals("Thunder") && CameraController._Instance.operatingstate == CameraController.Operatingstate.holdmagicball)
        {
            CameraController._Instance.Touchingball = null;
            return;
        }
        if (collision.gameObject.name.Equals("Thunder") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            CameraController._Instance.Touchingball = null;
            //CameraController._Instance.spells = null;
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Idle;
            Debug.Log("玩家狀態切換到 = " + CameraController._Instance.operatingstate);
        }
        if (collision.gameObject.name.Equals("IceWall") && CameraController._Instance.operatingstate == CameraController.Operatingstate.holdmagicball)
        {
            CameraController._Instance.Touchingball = null;
            return;
        }
        if (collision.gameObject.name.Equals("IceWall") && CameraController._Instance.operatingstate == CameraController.Operatingstate.Touchball)
        {
            CameraController._Instance.Touchingball = null;
            //CameraController._Instance.spells = null;
            CameraController._Instance.operatingstate = CameraController.Operatingstate.Idle;
            Debug.Log("玩家狀態切換到 = " + CameraController._Instance.operatingstate);
        }
    }
}
