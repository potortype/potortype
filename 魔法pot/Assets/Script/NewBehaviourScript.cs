﻿using UnityEngine;
using System.Collections;

public class NewBehaviourScript : MonoBehaviour {
    public Camera cameronfloor;
    RaycastHit RHWall;
    RaycastHit RHTarget;
    RaycastHit RHLine;
    float raycastlongth = 100.0f;
    public GameObject balls;
    GameObject ball;
    public GameObject wall;
    public GameObject Target;
    public GameObject target2;
    public bool ShowRay = false;
    public bool ShowAim = true;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //Ray landRayLine = new Ray(cameronfloor.transform.position, cameronfloor.transform.forward);
        //bool prehitLine = Physics.Raycast(landRayLine, out RHLine, raycastlongth);
        //if (prehitLine)
        //{
        //    Debug.DrawLine(cameronfloor.transform.position, RHLine.point, Color.green);
        //}

        Ray landRay = new Ray(cameronfloor.transform.position, cameronfloor.transform.forward);
        bool prehitWall = Physics.Raycast(landRay, out RHWall, raycastlongth, 1 << LayerMask.NameToLayer("Wall") | 1 << LayerMask.NameToLayer("Target") | 1 << LayerMask.NameToLayer("Terrain"));
       
        if (prehitWall)
        {
            if (ShowRay)
            {
                Debug.DrawLine(cameronfloor.transform.position, RHWall.point, Color.red);
            }
            if (ShowAim)
            {
                if (ball == null)
                {
                    ball = Instantiate(balls);
                }
                if (ball != null)
                {
                    ball.transform.position = RHWall.point;
                    ball.GetComponent<Renderer>().material.color = Color.green;
                }
            }
            else
            {
                if (ball != null)
                {
                    Destroy(ball);
                    ball = null;
                }
            }            
            if (wall != null && RHWall.collider.name.Equals("Wall1"))
            {
                Debug.Log("牆壁變色");
                //Debug.Log("RH1.collider.gameObject" + RH1.collider.gameObject.name);
                //wall = RH1.collider.gameObject;
                wall.GetComponent<Renderer>().material.color = new Color(1.0f, 0.0f, 0.0f);
                Target.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                target2 = wall;
                Debug.Log("目標是 = " + target2);
            }
            else
            {
                Debug.Log("沒有牆壁");
                Target.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
            }
            if (Target != null && RHWall.collider.name.Equals("Target"))
            {
                Debug.Log("目標變色");
                //Debug.Log("RH1.collider.gameObject" + RH1.collider.gameObject.name);
                //Target = RH1.collider.gameObject;
                target2 = Target;
                Debug.Log("目標是 = " + target2);
                Target.GetComponent<Renderer>().material.color = new Color(1.0f, 0.0f, 0.0f);
                wall.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
            }
            else
            {
                Debug.Log("沒有目標");
            }
            if (RHWall.collider.name.Equals("G2"))
            {
                if (ShowRay)
                {
                    Debug.DrawLine(cameronfloor.transform.position, RHWall.point, Color.green);
                }
                Debug.Log("打到地形");
                wall.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                Target.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
                target2 = null;

                if (ball != null)
                {
                    ball.transform.position = RHWall.point;
                    ball.GetComponent<Renderer>().material.color = Color.red;
                }
            }
            //prehitTarget = false;
        }
        else if (!prehitWall)
        {


            Debug.Log("沒打到任何東西");            
            wall.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);           
            Target.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f);
            target2 = null;
            if(ball != null)
            {
                Destroy(ball);
                ball = null;
            }
        }
        
    }
    
}
