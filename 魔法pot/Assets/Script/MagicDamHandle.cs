﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MagicDamHandle : MonoBehaviour
{
    static public MagicDamHandle _Instance;
    public int fireballSdam = 100;
    public int fireballLdam = 150;
    public int waterballSdam = 100;
    public int waterballLdam = 150;
    public int lightballSdam = 200;
    public int lightballLdam = 300;
    public int darkballSdam = 200;
    public int darkballLdam = 300;

    public int PhoenixSdam = 400;
    public int PhoenixLdam = 600;

    public int HammerSdam = 300;
    public int HammerLdam = 400;
    //落雷沒傷害
    public float normalShockTime = 2.0f;
    public float plusShockTime = 3.0f;

    public int IceWallSdam = 800;
    public int IceWallLdam = 900;


    public int firepoint = 2;
    public int waterpoint = 2;
    public int lightpoint = 3;
    public int darkpoint = 3;
    public int Phoenixpoint = 4;
    public int Hammerpoint = 6;
    public int IceWallpoint = 5;
    public int Thunderpoint = 6;

    public float normalfireballtime = 1.0f;
    public float plusfireballtime = 1.5f;
    public float normalwaterballtime = 1.0f;
    public float pluswaterballtime = 1.5f;
    public float normallightballtime = 1.5f;
    public float pluslightballtime = 1.8f;
    public float normaldarkballtime = 1.5f;
    public float plusdarkballtime = 1.8f;

    public float normalIceWalltime = 0.5f;
    public float plusIceWalltime = 0.7f;

    public float normalPhoenixtime = 0.5f;
    public float plusPhoenixtime = 0.7f;

    public float normalHammertime = 2.0f;
    public float plusHammertime = 2.2f;

    public float normalThundertime = 1.0f;
    public float plusThundertime = 1.2f;

    //public int IceWallReducefire = 30;
    //public int IceWallReducewater = 80;
    //public int IceWallReducePhoenix = 40;
    //public int IceWallReduceHammer = 60;
    //public int IceWallReducelight = 10;
    //public int IceWallReducedark = 10;
    public float spellspeed = 10.0f;
    public float maxspellspeed = 20.0f;
    public float maxRotate = 15.0f;
    public float maxpredictionspeed = 6.0f;
    public List<GameObject> allRBpathmagic;
    // Use this for initialization
    void Awake()
    {
        _Instance = this;
        allRBpathmagic = new List<GameObject>();
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
