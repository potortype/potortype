﻿using UnityEngine;
using System.Collections;

public class Energywave : MonoBehaviour
{
    public enum Spellspecies
    {
        Idle,
        SetFireBall,
        FireBall,
        SetIceSpear,
        IceSpear,
        SetPhoenix,
        Phoenix,
        SetIceWall,
        IceWall,
        SetThunder,
        Thunder,
        SetHammer,
        Hammer,
        SetLightBall,
        LightBall,
        SetDarkBall,
        DarkBall
    }
    public Spellspecies spellspecies;
    public float count = 0.0f;
    public float maxpowerfadetime;
    public float maxpowerscaletime;
    public Vector3 scale;
    //儲存抓到的法術RGB
    public float r;
    public float g;
    public float b;
    public float a;
    //原始大小
    public Vector3 fscale;
    //最大大小
    public Vector3 tscale;
    public ushort maxvibrate = 1000;
    public ushort vibrate = 200;
    //public GameObject ControllerVibrate;
    public SteamVR_TrackedObject trackedObj;
    // Use this for initialization
    void Start()
    {
        //紀錄原始大小，先做死放大到兩倍，之後看情況調整
        fscale = gameObject.transform.localScale;
        tscale = gameObject.transform.localScale * 2.0f;
        //Debug.Log("gameObject.transform.localScale = " + fscale);
        


    }

    // Update is called once per frame
    void Update()
    {
        if(trackedObj == null)
        {
            trackedObj = gameObject.transform.parent.gameObject.GetComponent<SteamVR_TrackedObject>();
        }

        //Debug.Log("spellspecies = " + spellspecies);

        a = gameObject.GetComponent<Renderer>().material.color.a;
        scale = gameObject.transform.localScale;

        if (spellspecies == Spellspecies.Idle)
        {
            count = 0.0f;
        }
        if (spellspecies == Spellspecies.SetFireBall)
        {
            count = 0.0f;
            maxpowerfadetime = MagicDamHandle._Instance.plusfireballtime;
            maxpowerscaletime = MagicDamHandle._Instance.plusfireballtime;
            spellspecies = Spellspecies.FireBall;
        }
        if (spellspecies == Spellspecies.FireBall)
        {
            if (count <= maxpowerfadetime)
            {
                //淡入
                //Debug.Log("淡入 = " + a + "放大 = " + s);
                count += Time.deltaTime;
                a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
                scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
                gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
                gameObject.transform.localScale = scale;

                //Fadein();

            }
            //else if (count >= maxpowerfadetime)
            //{

            //    //回到初始
            //    //Debug.Log("回到初始值a = " + a + "s = " + s);
            //    count = 0.0f;
            //    a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
            //    scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
            //    gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
            //    gameObject.transform.localScale = scale;
            //    //Setinitial();

            //}
            if (count <= MagicDamHandle._Instance.normalfireballtime)
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(vibrate);
                }
            }
            else
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(maxvibrate);
                }

            }

        }
        if (spellspecies == Spellspecies.SetIceSpear)
        {
            count = 0.0f;
            maxpowerfadetime = MagicDamHandle._Instance.pluswaterballtime;
            maxpowerscaletime = MagicDamHandle._Instance.pluswaterballtime;
            spellspecies = Spellspecies.IceSpear;
        }
        if (spellspecies == Spellspecies.IceSpear)
        {
            if (count <= maxpowerfadetime)
            {
                //淡入
                //Debug.Log("淡入 = " + a + "放大 = " + s);
                count += Time.deltaTime;
                a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
                scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
                gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
                gameObject.transform.localScale = scale;

            }
            if (count <= MagicDamHandle._Instance.normalwaterballtime)
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(vibrate);
                }
            }
            else
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(maxvibrate);
                }

            }
            //else if (count >= maxpowerfadetime)
            //{
            //    //回到初始
            //    //Debug.Log("回到初始值a = " + a + "s = " + s);
            //    count = 0.0f;
            //    a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
            //    scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
            //    gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
            //    gameObject.transform.localScale = scale;

            //}
        }
        if (spellspecies == Spellspecies.SetPhoenix)
        {
            count = 0.0f;
            maxpowerfadetime = MagicDamHandle._Instance.plusPhoenixtime;
            maxpowerscaletime = MagicDamHandle._Instance.plusPhoenixtime;
            spellspecies = Spellspecies.Phoenix;

        }
        if (spellspecies == Spellspecies.Phoenix)
        {
            if (count <= maxpowerfadetime)
            {
                //淡入
                //Debug.Log("淡入 = " + a + "放大 = " + s);
                count += Time.deltaTime;
                a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
                scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
                gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
                gameObject.transform.localScale = scale;

            }
            if (count <= MagicDamHandle._Instance.normalPhoenixtime)
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(vibrate);
                }
            }
            else
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(maxvibrate);
                }

            }
            //else if (count >= maxpowerfadetime)
            //{
            //    //回到初始
            //    //Debug.Log("回到初始值a = " + a + "s = " + s);
            //    count = 0.0f;
            //    a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
            //    scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
            //    gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
            //    gameObject.transform.localScale = scale;

            //}
        }
        if (spellspecies == Spellspecies.SetIceWall)
        {
            count = 0.0f;
            maxpowerfadetime = MagicDamHandle._Instance.plusIceWalltime;
            maxpowerscaletime = MagicDamHandle._Instance.plusIceWalltime;
            spellspecies = Spellspecies.IceWall;

        }
        if (spellspecies == Spellspecies.IceWall)
        {
            if (count <= maxpowerfadetime)
            {
                //淡入
                //Debug.Log("淡入 = " + a + "放大 = " + s);
                count += Time.deltaTime;
                a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
                scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
                gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
                gameObject.transform.localScale = scale;

            }
            if (count <= MagicDamHandle._Instance.normalIceWalltime)
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(vibrate);
                }
            }
            else
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(maxvibrate);
                }

            }
            //else if (count >= maxpowerfadetime)
            //{
            //    //回到初始
            //    //Debug.Log("回到初始值a = " + a + "s = " + s);
            //    count = 0.0f;
            //    a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
            //    scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
            //    gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
            //    gameObject.transform.localScale = scale;

            //}
        }
        if (spellspecies == Spellspecies.SetThunder)
        {
            count = 0.0f;
            maxpowerfadetime = MagicDamHandle._Instance.plusThundertime;
            maxpowerscaletime = MagicDamHandle._Instance.plusThundertime;
            spellspecies = Spellspecies.Thunder;

        }
        if (spellspecies == Spellspecies.Thunder)
        {
            if (count <= maxpowerfadetime)
            {
                //淡入
                //Debug.Log("淡入 = " + a + "放大 = " + s);
                count += Time.deltaTime;
                a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
                scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
                gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
                gameObject.transform.localScale = scale;

            }
            if (count <= MagicDamHandle._Instance.normalThundertime)
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(vibrate);
                }
            }
            else
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(maxvibrate);
                }

            }
            //else if (count >= maxpowerfadetime)
            //{
            //    //回到初始
            //    //Debug.Log("回到初始值a = " + a + "s = " + s);
            //    count = 0.0f;
            //    a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
            //    scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
            //    gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
            //    gameObject.transform.localScale = scale;

            //}
        }
        if (spellspecies == Spellspecies.SetHammer)
        {
            count = 0.0f;
            maxpowerfadetime = MagicDamHandle._Instance.plusHammertime;
            maxpowerscaletime = MagicDamHandle._Instance.plusHammertime;
            spellspecies = Spellspecies.Hammer;

        }
        if (spellspecies == Spellspecies.Hammer)
        {
            if (count <= maxpowerfadetime)
            {
                //淡入
                //Debug.Log("淡入 = " + a + "放大 = " + s);
                count += Time.deltaTime;
                a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
                scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
                gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
                gameObject.transform.localScale = scale;

            }
            if (count <= MagicDamHandle._Instance.normalHammertime)
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(vibrate);
                }
            }
            else
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(maxvibrate);
                }

            }
            //else if (count >= maxpowerfadetime)
            //{
            //    //回到初始
            //    //Debug.Log("回到初始值a = " + a + "s = " + s);
            //    count = 0.0f;
            //    a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
            //    scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
            //    gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
            //    gameObject.transform.localScale = scale;

            //}
        }
        if (spellspecies == Spellspecies.SetLightBall)
        {
            count = 0.0f;
            maxpowerfadetime = MagicDamHandle._Instance.pluslightballtime;
            maxpowerscaletime = MagicDamHandle._Instance.pluslightballtime;
            spellspecies = Spellspecies.LightBall;

        }
        if (spellspecies == Spellspecies.LightBall)
        {
            if (count <= maxpowerfadetime)
            {
                //淡入
                //Debug.Log("淡入 = " + a + "放大 = " + s);
                count += Time.deltaTime;
                a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
                scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
                gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
                gameObject.transform.localScale = scale;

            }
            if (count <= MagicDamHandle._Instance.normallightballtime)
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(vibrate);
                }
            }
            else
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(maxvibrate);
                }

            }
            //else if (count >= maxpowerfadetime)
            //{
            //    //回到初始
            //    //Debug.Log("回到初始值a = " + a + "s = " + s);
            //    count = 0.0f;
            //    a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
            //    scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
            //    gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
            //    gameObject.transform.localScale = scale;

            //}
        }
        if (spellspecies == Spellspecies.SetDarkBall)
        {
            count = 0.0f;
            maxpowerfadetime = MagicDamHandle._Instance.plusdarkballtime;
            maxpowerscaletime = MagicDamHandle._Instance.plusdarkballtime;
            spellspecies = Spellspecies.DarkBall;

        }
        if (spellspecies == Spellspecies.DarkBall)
        {
            if (count <= maxpowerfadetime)
            {
                //淡入
                //Debug.Log("淡入 = " + a + "放大 = " + s);
                count += Time.deltaTime;
                a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
                scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
                gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
                gameObject.transform.localScale = scale;

            }
            if (count <= MagicDamHandle._Instance.normaldarkballtime)
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(vibrate);
                }
            }
            else
            {
                if (trackedObj != null)
                {
                    if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                    {
                        return;
                    }

                    SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(maxvibrate);
                }

            }
            //else if (count >= maxpowerfadetime)
            //{
            //    //回到初始
            //    //Debug.Log("回到初始值a = " + a + "s = " + s);
            //    count = 0.0f;
            //    a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
            //    scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
            //    gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
            //    gameObject.transform.localScale = scale;

            //}
        }



    }
    void Fadein()
    {
        count += Time.deltaTime;
        a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
        scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
        gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
        gameObject.transform.localScale = scale;
    }
    void Setinitial()
    {
        count = 0.0f;
        a = Mathf.Lerp(0, 1, (count / maxpowerfadetime));
        scale = Vector3.Lerp(fscale, tscale, count / maxpowerscaletime);
        gameObject.GetComponent<Renderer>().material.color = new Color(r, g, b, a);
        gameObject.transform.localScale = scale;
    }
    //public ushort vibrateCrescendo(float totaltime,float cutzoon, ushort crescendoamplitude)
    //{
    //    float cuttime = totaltime / cutzoon;

    //    if(totaltime <= cuttime * count)
    //    {
    //        currentvibrate = vibrate;
    //        return vibrate;
    //    }else if(cuttime <= totaltime && totaltime <= cuttime * 2.0f)
    //    {
    //        currentvibrate += crescendoamplitude;
    //        return currentvibrate;
    //    }else if (cuttime * 2.0f <= totaltime && totaltime <= cuttime * 3.0f)
    //    {
    //        currentvibrate += crescendoamplitude;
    //        return currentvibrate;
    //    }else if (cuttime * 3.0 <= totaltime && totaltime <= cuttime * 4.0f)
    //    {
    //        currentvibrate += crescendoamplitude;
    //        return currentvibrate;
    //    }else if (cuttime * 4.0 <= totaltime && totaltime <= cuttime * 5.0f)
    //    {
    //        currentvibrate += crescendoamplitude;
    //        return currentvibrate;
    //    }
    //    return maxvibrate;

    //}
}
