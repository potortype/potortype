﻿using UnityEngine;
using System.Collections;

public class VRMove : MonoBehaviour {
    public enum Playerstate
    {
        OnFloor,
        Seepoint,
        Air,
    }
    public Playerstate playerstate;
    // Use this for initialization
    void Start () {
        playerstate = Playerstate.OnFloor;
    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log("玩家狀態" + playerstate);
    }
}
